package demo

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row}




/**
  * @date: 2019/6/27
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: Spark 三大数据模型的比较
  */
case class Other(id: Int, name: String)

case class Person(val id: Int, val name: String, val gender: String, val amount: Double)

/*

object DataSet_DataFrame_Rdd {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)


    val spark = SparkUtils.getSparkSession("")
    import spark.implicits._

    val rdd: RDD[String] = spark.sparkContext.textFile("userprofile/demodata/graphx/input/person.csv")

    val rdd2: RDD[Person] = rdd.map(line => {
      val arr: Array[String] = line.split(",")
      Person(arr(0).toInt, arr(1), arr(2), arr(3).toDouble)
    })

    rdd2.take(10).foreach(println)


    /**
      * dataset 与 rdd的区别：  dataset拥有schema，而且dataset拥有sql的api
      */
    val ds1: Dataset[Person] = rdd2.toDS()
    ds1.printSchema()
    ds1.show(10, false)

    // ds 拥有表结构，而且ds拥有sql 的api
    val df2: DataFrame = ds1.where("gender='F'").select("id", "name")


    /**
      * 看dataset和dataframe的区别
      */
    val df1: DataFrame = ds1.toDF()
    df1.map(row => {
      val name: String = row.getAs[String]("name")
      // 基于通用Row类型，字段的类型已经失去了强约束
      val amount: Boolean = row.getAs[Boolean]("amount") // 此类型不匹配的错误，只有在运行时才能发现
      (name, amount)
    }).toDF("name", "amount")
      .show(10, false)

    ds1.map(p => {
      val name: String = p.name
      val amount: Double = p.amount // 直接基于原生类型，变量本身就有类型强约束
    })

    spark.close()
  }

}
*/
