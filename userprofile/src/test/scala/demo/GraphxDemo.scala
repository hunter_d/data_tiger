package demo

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset

/**
  * @date: 2019/6/26
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 图计算入门demo
  */
object GraphxDemo {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkUtils.getSparkSession("graphxdemo")
    import spark.implicits._

    /**
      * 1,13811,wx_a,刘德华
      * 2,13812,wx_a,华仔
      * 3,13812,wx_b,华华
      * 4,13821,wx_d,angelababy
      * 5,13821,wx_c,杨颖
      * 6,13823,wx_c,天宝
      */


    val ds = spark.read.textFile("userprofile/demodata/graphx/input")

    ds.show(10, false)

    // 构造点集合
    val vertextRDD: RDD[(Long, String)] = ds.flatMap(line => {

      val arr = line.split(",")

      arr.map(id => (id.hashCode.toLong, id))

    }).rdd

    //vertextRDD.take(30).foreach(println)


    // 构造边集合
    val edgeRDD: RDD[Edge[String]] = ds.flatMap(line => {

      val arr = line.split(",")

      var edgeLst = List.empty[Edge[String]]

      for (i <- 0 until arr.length - 1; j <- i + 1 until (arr.length)) {
        edgeLst :+= Edge(arr(i).hashCode.toLong, arr(j).hashCode.toLong, "")
      }

      edgeLst

    }).rdd

    // 构造图
    val graph: Graph[String, String] = Graph(vertextRDD, edgeRDD)

    // 求连通子图
    val res: Graph[VertexId, String] = graph.connectedComponents()


    val resVertices = res.vertices


    // 最后得到如下结果形式：
    // 49,13811,wx_a,刘德华,13812,wx_a,华仔,wx_b,华华
    // -1384493738,13821,wx_d,angelababy,wx_c,杨颖,13823,天宝
    resVertices.join(vertextRDD)
      .map(_._2)
      .distinct()
      .groupByKey()
      .map(tp=>(tp._1,tp._2.toList))
      .take(40)
      .foreach(println)


    spark.close()


  }

}
