package demo

import cn.doitedu.common.utils.SparkUtils

object ShowParquet {

  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("")
    spark.read.parquet("userprofile/tags_out/doit_tag_score")
      /*.where("logType = 'pg_view'")*/
      .show(10,false)

    spark.close()
  }

}
