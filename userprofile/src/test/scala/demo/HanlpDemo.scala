package demo

import java.util

import com.hankcs.hanlp.HanLP
import com.hankcs.hanlp.seg.common.Term

object HanlpDemo {

  def main(args: Array[String]): Unit = {

    // 简繁转换
    val str = HanLP.convertToTraditionalChinese("我爱你中国")
    println(str)


    // 抽取关键词
    val doc = "偷看女士洗澡，偷窥男被当场抓住，结果双方居然还是熟人。近日，这幕尴尬场面发生在余杭星桥街道某工地。 王女士(化名)，今年44岁，和老公一起在余杭星桥街道某工地开了一间杂货店，夫妻俩平时除了搞搞工地卫生，就是照看店里的生意，日常起居都是在工地宿舍内。 6月23日22时许，王女士从店里下班后，到工地里的女公共浴室洗澡，可洗着洗着，抬头一看，突然看到有一名40岁左右穿着灰色短袖的男子在窗户边偷看她，王女士虽略有惊慌后立即冷静下来，一边迅速穿好衣服，一边高声喊老公。男子见行迹败露，慌忙朝工地另一端生活区逃窜，王女士夫妇紧追不放。"
    val res: util.List[String] = HanLP.extractKeyword(doc,6)
    println(res)


    // 抽取摘要
    val summary = HanLP.extractSummary(doc,3)
    println(summary)


    // 分词
    val terms: util.List[Term] = HanLP.segment(doc)
    import scala.collection.JavaConversions._
    val words = for(t <- terms) yield t.word
    println(words)


  }

}
