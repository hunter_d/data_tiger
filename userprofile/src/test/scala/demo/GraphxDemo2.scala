package demo

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph, VertexId, VertexRDD}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * @date: 2019/6/27
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 图计算示例2
  */
object GraphxDemo2 {


  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkSession.builder().appName("graphx demo2").master("local[*]").getOrCreate()
    import spark.implicits._

    // 读取带schema的csv文件
    val df = spark.read.option("header", "true").csv("userprofile/demodata/graphx/input/ershixiong.txt")

    // 生成点集合
    val vertices: RDD[(VertexId, String)] = df.rdd.flatMap(row => {

      val phone = row.getAs[String]("phone")
      val name = row.getAs[String]("name")
      val wx = row.getAs[String]("wx")

      for (elem <- Array(phone, name, wx) if elem != null) yield (elem.hashCode.toLong, elem)
    })


    // 生成边集合
    val edges: RDD[Edge[String]] = df.rdd.flatMap(row => {

      val phone = row.getAs[String]("phone")
      val name = row.getAs[String]("name")
      val wx = row.getAs[String]("wx")

      val ids = Array(phone, name, wx).filter(_ != null)

      for (i <- 1 until ids.length) yield Edge(ids(0).hashCode.toLong, ids(i).hashCode.toLong, "")

    })


    val graph = Graph(vertices, edges)

    // (刘德华.hashcode.toLong,组号)
    val groupedIds: RDD[(Long,Long)] = graph.connectedComponents().vertices


    // 将id映射结果收集为hashmap
    val idmapingDict: collection.Map[VertexId, VertexId] = groupedIds.collectAsMap()

    // 将id映射字典作为广播变量广播出去
    val bc = spark.sparkContext.broadcast[collection.Map[Long, Long]](idmapingDict)

    // 对原数据标记组id
    val resrdd = df.rdd.map(row => {
      val dict: collection.Map[VertexId, VertexId] = bc.value

      val phone = row.getAs[String]("phone")
      val name = row.getAs[String]("name")
      val wx = row.getAs[String]("wx")
      val sale = row.getAs[String]("sale").toInt


      // 用手机号、姓名、微信中任意一个非空的，去id映射字典中取组id
      val gid: VertexId = dict.get(Array(phone, name, wx).filter(_ != null)(0).hashCode.toLong).get

      (gid, phone, name, wx, sale)
    })

    // 给原始数据打上组id标记的结果
    val resdf = resrdd.toDF("gid", "phone", "name", "wx", "sale")
    resdf.show(10,false)


    // 整合每个人的所有ids
    resrdd.map(tp => (tp._1, (tp._2, tp._3, tp._4))).groupByKey().mapValues(it => {
      val hashset = new collection.mutable.HashSet[String]()
      it.map(tp => {
        hashset.add(tp._1)
        hashset.add(tp._2)
        hashset.add(tp._3)
      })

      hashset.filter(_!=null)
    })
      .map(tp => (tp._1, tp._2.mkString(",")))
      .toDF("gid", "ids")
      .show(10, false)


    spark.close()

  }


}
