import scala.collection.immutable

val arr = Array("a","b","c","d")

val map = Map("a"->1,"b"->2,"c"->3)

val m: Map[String, Int] = for((k,v)<-map) yield (k,v+10)

for((k,v)<-map) yield (v,k)




val res: immutable.Seq[String] = for(i<- 0 until arr.length-1; j <- 0 until arr.length)
  yield arr(i)+":"+arr(j)


println(res)