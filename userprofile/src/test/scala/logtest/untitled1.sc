val lst = List(
  ("g01","idstags","IME","im01",5.0d),
  ("g01","idstags","IME","im11",5.0d),
  ("g01","idstags","IMS","is01",6.0d),
  ("g01","apptags","APP","iqiyi",3.0d),
  ("g01","apptags","APP","qutoutiao",2.0d),
  ("g01","apptags","DEC","dec01",2.0d),
  ("g02","idstags","MAC","mc02",4.0d),
  ("g02","itemtags","CT1","ct101",2.0d)
)

val x = lst.groupBy(tp=>(tp._1,tp._2,tp._3)).map(tp=>{
  val t3 = tp._1
  val tm = tp._2.map(tp=>(tp._4,tp._5)).toMap
  (t3._1,t3._2,t3._3,tm)
})

val y = x.groupBy(tp=>(tp._1,tp._2)).map(tp=>{
  val t2 = tp._1
  val tm = tp._2.map(tp=>(tp._3,tp._4)).toMap
  (t2._1,t2._2,tm)

})

val z = y.groupBy(tp=>(tp._1)).map(tp=>{
  val t1 = tp._1
  val tm = tp._2.map(tp=>(tp._2,tp._3)).toMap
  (t1,tm)
})


println(z)