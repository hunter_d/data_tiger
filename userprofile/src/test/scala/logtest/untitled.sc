import org.json4s.jackson.JsonMethods._
import org.json4s.JsonDSL._

val lst = List(
  ("g01","idstags","IME","im001",6.0),
  ("g01","idstags","IME","im011",5.0),
  ("g01","apptags","APP","app01",6.0),
  ("g01","apptags","APP","app02",6.0),
  ("g01","apptags","DES","des02",6.0),
  ("g02","apptags","DES","des03",6.0)

)

val x = lst.groupBy(tp=>((tp._1,tp._2,tp._3)))
.map(tp=>{
  val t3 = tp._1
  val tm = tp._2.map(tp=>(tp._4,tp._5)).toMap
  (t3._1,t3._2,t3._3,tm)
})

    .groupBy(tp=>(tp._1,tp._2))
    .map(tp=>{
      val t2 = tp._1
      val tm = tp._2.map(tp=>(tp._3,tp._4)).toMap
      (t2._1,t2._2,tm)

    })
    .groupBy(tp=>(tp._1))
    .map(tp=>{
      val t1 = tp._1
      val tm = tp._2.map(tp=>(tp._2,tp._3)).toMap
      (t1,tm)
    })

val js = compact(render(x))
println(js)

println(x)