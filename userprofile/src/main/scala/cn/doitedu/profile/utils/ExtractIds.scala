package cn.doitedu.profile.utils

import com.alibaba.fastjson.JSON
import org.apache.commons.lang3.StringUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

/**
  * @date: 2019/6/29
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 抽取各类数据中的点集合，用于后续图计算
  */
object ExtractIds {

  /**
    * 抽取doit日志数据中的ids，返回点集合RDD
    *
    * @param spark
    * @param filePath
    * @return
    */
  def extractDoitLogIds(spark: SparkSession, filePath: String): RDD[(Long, String)] = {


    val raw = spark.read.textFile(filePath).rdd

    val tmp: RDD[Array[(Long, String)]] = raw.map(line => {
      val json = line.split(" --> ", -1)(1)
      try {
        val jsonObj = JSON.parseObject(json)

        val phone = jsonObj.getJSONObject("u").getString("phoneNbr")
        val phoneObj = jsonObj.getJSONObject("u").getJSONObject("phone")

        val imei = phoneObj.getString("imei")
        val deviceId = phoneObj.getString("deviceId")
        val androidId = phoneObj.getString("androidId")

        for (id <- Array(phone, imei, deviceId, androidId).filter(StringUtils.isNotBlank(_))) yield (id.hashCode.toLong, id)

      } catch {
        case _ => List.empty[(Long, String)].toArray
      }
    })
    tmp.filter(x => x.size > 0).flatMap(x => x)
  }

  /**
    * 抽取DSP日志数据中的ids，返回点集合RDD
    * imei       47
    * mac        48
    * idfa       49
    * openudid   50
    * androidid  51
    *
    * @param spark
    * @param filePath
    * @return
    */
  def extractDSPLogIds(spark: SparkSession, filePath: String): RDD[(Long, String)] = {

    val raw = spark.read.textFile(filePath).rdd
    raw
      .map(line => {
        line.split(",", -1)
      })
      .filter(_.size >= 85)
      .flatMap(arr => {
        val imei = arr(47)
        val mac = arr(48)
        val idfa = arr(49)
        val openudid = arr(50)
        val android = arr(51)
        for (id <- Array(imei, mac, idfa, openudid, android).filter(StringUtils.isNotBlank(_))) yield (id.hashCode.toLong, id)
      })
  }


  /**
    * 抽取CMCC日志数据中的ids，返回点集合RDD
    *
    * @param spark
    * @param filePath
    * @return
    */
  def extractCMCCLogIds(spark: SparkSession, filePath: String): RDD[(Long, String)] = {
    val raw = spark.read.textFile(filePath).rdd.map(_.split("\t", -1)).filter(_.size >= 49)
    raw.flatMap(arr => {

      val phone = arr(6)
      val imei = arr(7)
      val imsi = arr(8)

      for (id <- Array(phone, imei, imsi).filter(StringUtils.isNotBlank(_))) yield (id.hashCode.toLong, id)
    })

  }
}
