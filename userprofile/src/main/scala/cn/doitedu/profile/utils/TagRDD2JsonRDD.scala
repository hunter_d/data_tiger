package cn.doitedu.profile.utils

import cn.doitedu.profile.tagbeans.TagBean
import com.alibaba.fastjson.JSON
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row}

import scala.collection.JavaConverters
import scala.collection.parallel.immutable

object TagRDD2JsonRDD {

  def tag2Json(aggedTags:RDD[Row]):RDD[String] = {

    // 拼装Map结构  Map[组id, Map[标签模块名, Map[标签名, Map[标签值, 权重]]]]
    val beanRDD: RDD[TagBean] = aggedTags.map(row => {

      val gid = row.getAs[String]("gid")
      val tagmodule = row.getAs[String]("tagmodule")
      val tagname = row.getAs[String]("tagname")
      val tagvalue = row.getAs[String]("tagvalue")
      val score = row.getAs[Double]("score")

      (gid, tagmodule, tagname, tagvalue, score)
    })

      // 将同一个人的所有标签分到一组中
      .groupBy(_._1)
      // 将每一个人的所有标签拼接map
      .map(tp => {

      val lst = tp._2.toList

      val x = lst.groupBy(tp => (tp._1, tp._2, tp._3)).map(tp => {
        val t3 = tp._1
        val tm = tp._2.map(tp => (tp._4, tp._5)).toMap
        val tmj: java.util.Map[String, Double] = JavaConverters.mapAsJavaMapConverter(tm).asJava

        (t3._1, t3._2, t3._3, tmj)
      })

      val y = x.groupBy(tp => (tp._1, tp._2)).map(tp => {
        val t2 = tp._1
        val tm = tp._2.map(tp => (tp._3, tp._4)).toMap
        val tmj = JavaConverters.mapAsJavaMapConverter(tm).asJava
        (t2._1, t2._2, tmj)

      })

      val z = y.groupBy(tp => (tp._1)).map(tp => {
        val t1 = tp._1
        val tm = tp._2.map(tp => (tp._2, tp._3)).toMap
        val tmj = JavaConverters.mapAsJavaMapConverter(tm).asJava

        //(t1, tm)
        TagBean(t1, tmj)
      })
      z.toList(0)
    })

    val jsonRdd = beanRDD.map(bean => {

      JSON.toJSONString(bean,false)

    })

    jsonRdd

  }

}
