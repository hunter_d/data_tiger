package cn.doitedu.profile.utils

import com.alibaba.fastjson.JSON
import org.apache.commons.lang3.StringUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import scala.collection.immutable


/**
  * @date: 2019/6/29
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 抽取各类数据中的id两两对，用于后续的图计算
  */
object ExtractIDPair {


  /**
    * 抽取doit日志数据中的id并拼接成两两对
    *
    * @param spark
    * @param filePath
    * @return
    */
  def extractDoitLogIDPair(spark: SparkSession, filePath: String): RDD[(Long, Long)] = {

    val raw = spark.read.textFile(filePath).rdd

    raw.map(line => {
      val json = line.split(" --> ", -1)(1)
      try {
        val jsonObj = JSON.parseObject(json)

        val phone = jsonObj.getJSONObject("u").getString("phoneNbr")
        val phoneObj = jsonObj.getJSONObject("u").getJSONObject("phone")

        val imei = phoneObj.getString("imei")
        val deviceId = phoneObj.getString("deviceId")
        val androidId = phoneObj.getString("androidId")

        val array = Array(phone, imei, deviceId, androidId).filter(StringUtils.isNotBlank(_))
        for (i <- 0 until array.size - 1; j <- i + 1 until array.size) yield (array(i).hashCode.toLong, array(j).hashCode.toLong)
      } catch {
        case _ => List.empty[(Long, Long)].toSeq
      }
    })
      .filter(_.size > 0).flatMap(x => x)
  }


  /**
    * 抽取DSP日志数据中的id并拼接成两两对
    *
    * @param spark
    * @param filePath
    * @return
    */
  def extractDSPLogIDPair(spark: SparkSession, filePath: String): RDD[(Long, Long)] = {

    val raw = spark.read.textFile(filePath).rdd
    raw
      .map(line => {
        line.split(",", -1)
      })
      .filter(_.size >= 85)
      .flatMap(arr => {
        val imei = arr(47)
        val mac = arr(48)
        val idfa = arr(49)
        val openudid = arr(50)
        val android = arr(51)
        val array = Array(imei, mac, idfa, openudid, android).filter(StringUtils.isNotBlank(_))

        for (i <- 0 until array.size - 1; j <- i + 1 until array.size) yield (array(i).hashCode.toLong, array(j).hashCode.toLong)
      })
  }


  /**
    * 抽取DSP日志数据中的id并拼接成两两对
    *
    * @param spark
    * @param filePath
    * @return
    */
  def extractCMCCLogIDPair(spark: SparkSession, filePath: String): RDD[(Long, Long)] = {

    val raw = spark.read.textFile(filePath).rdd.map(_.split("\t", -1)).filter(_.size >= 49)
    raw.flatMap(arr => {

      val phone = arr(6)
      val imei = arr(7)
      val imsi = arr(8)

      val array = Array(phone, imei, imsi).filter(StringUtils.isNotBlank(_))
      for (i <- 0 until array.size - 1; j <- i + 1 until array.size) yield (array(i).hashCode.toLong, array(j).hashCode.toLong)

    })

  }


}
