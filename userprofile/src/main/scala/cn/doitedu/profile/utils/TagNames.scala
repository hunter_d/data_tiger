package cn.doitedu.profile.utils

object TagNames {

  val IMEI:String = "IME"
  val IMSI:String = "IMS"
  val MAC:String = "MAC"
  val ANDORID_ID:String = "ADR"
  val PROVINCE:String = "PRO"
  val CITY:String = "CIT"

}
