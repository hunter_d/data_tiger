package cn.doitedu.profile.dspkpi

import java.util.Properties

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{DataFrame, Row, SaveMode}

/**
  * @date: 2019/6/29
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: DSP业务kpi事件概况报表
  */
object DspKpiReports {
  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)


    val spark = SparkUtils.getSparkSession("dspkpi")
    import org.apache.spark.sql.functions._
    import spark.implicits._


    val dsplog = spark.read.parquet("userprofile/preout/dspout")


    // DSL风格查询语法 来自Pandas语言，和R语言
    val tmp: DataFrame = dsplog.select(
      col("provincename")
      , col("cityname")
      , col("requestmode")
      , $"processnode"
      , 'iseffective
      , 'isbilling
      , 'isbid
      , 'iswin
      , 'adorderid
      , 'winPrice
      , 'adpayment
    )

    // RDD 方式
    tmp.rdd
      .map({

        case Row(provincename: String, cityname: String, requestmode: Int, processnode: Int, iseffective: Int, isbilling: Int, isbid: Int, iswin: Int, adorderid: Int, winPrice: Double, adpayment: Double)
        => {
          var is_Orgin = 0
          var is_Effective = 0
          var is_AdReq = 0
          var is_Bid = 0
          var is_BidSucc = 0
          var is_Show = 0
          var is_Click = 0
          var pay: Double = 0
          var cost: Double = 0

          if (requestmode == 1 && processnode >= 1) is_Orgin = 1
          if (requestmode == 1 && processnode >= 2) is_Effective = 1
          if (requestmode == 1 && processnode == 3) is_AdReq = 1

          if (iseffective == 1 && isbilling == 1 && isbid == 1 && adorderid != 0) is_Bid = 1
          if (iseffective == 1 && isbilling == 1 && iswin == 1) is_BidSucc = 1
          if (requestmode == 2 && iseffective == 1) is_Show = 1
          if (requestmode == 3 && iseffective == 1) is_Click = 1
          if (iseffective == 1 && isbilling == 1 && iswin == 1) {
            pay = winPrice / 1000d;
            cost = adpayment / 1000d
          }

          ((provincename, cityname), (is_Orgin, is_Effective, is_AdReq, is_Bid, is_BidSucc, is_Show, is_Click, pay, cost))
        }


      })
      .groupByKey
      .mapValues(iter => {
        iter.reduce((x, y) => {
          (x._1 + y._1, x._2 + y._2, x._3 + y._3, x._4 + y._4, x._5 + y._5, x._6 + y._6, x._7 + y._7, x._8 + y._8, x._9 + y._9)
        })

      })
      .map(tp => {

        (tp._1._1, tp._1._2, tp._2._1, tp._2._2, tp._2._3, tp._2._4, tp._2._5, tp._2._6, tp._2._7, tp._2._8, tp._2._9)

      })
      .toDF("provincename", "cityname", "originReqCnts", "effectiveReqCnts", "adReqCnts", "bidReqCnts", "bidSuccCnts", "showCnts", "clickCnts", "payAmt", "costAmt")

      .show(10, false)



    // SQL 方式
    tmp.createTempView("dsp")


    val res = spark.sql(
      """
        |
        |select
        |provincename as provinceName,
        |cityname as cityName,
        |sum(if(requestmode=1 and processnode >= 1,1,0)) as rq,
        |sum(if(requestmode = 1 and processnode >= 2,1,0)) as avRq,
        |sum(if (requestmode = 1 and processnode = 3,1,0)) as adRq,
        |sum(if (iseffective = 1 and isbilling = 1 and isbid = 1 and adorderid != 0,1,0) ) as bidRq,
        |sum(if (iseffective = 1 and isbilling = 1 and iswin = 1,1,0)) as bidSuc,
        |sum(if (requestmode = 2 and iseffective = 1,1,0)) as adShow,
        |sum(if (requestmode = 3 and iseffective = 1,1,0)) as adClick,
        |sum(if (iseffective = 1 and isbilling = 1 and iswin = 1,winPrice/1000,0)) as showConsume,
        |sum(if (iseffective = 1 and isbilling = 1 and iswin = 1,adpayment/1000,0)) as showCost
        |from dsp
        |group by provincename,cityname
        |
      """.stripMargin)
    res.show(10, false)


    // res.write.parquet("")
    // 将报表直接写入mysql
    val props = new Properties()
    props.setProperty("user","root")
    props.setProperty("driver","com.mysql.jdbc.Driver")
    props.setProperty("password","root")
    res.write.mode(SaveMode.Append).jdbc("jdbc:mysql://localhost:3306/dmp?useUnicode=true&characterEncoding=utf-8","dws_kpi_rpt_area",props)
    spark.close()


  }
}
