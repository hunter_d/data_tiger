package cn.doitedu.profile.idmdict

import cn.doitedu.common.utils.SparkUtils
import cn.doitedu.profile.utils.{ExtractIDPair, ExtractIds}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Dataset

/**
  * @date: 2019/6/27
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 常规的滚动计算id映射字典生成程序
  *               既要考虑当天日志数据中的用户的各id标识，也要考虑上一日的idm字典
  */
object IdMappingRegular {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkUtils.getSparkSession("graphxdemo")
    import spark.implicits._

    // 抽取doitlog中id
    val doitIds = ExtractIds.extractDoitLogIds(spark,"userprofile/testdata/doitlog_day01")


    // 抽取DSP日志中的ids
    val dspIds = ExtractIds.extractDSPLogIds(spark,"userprofile/testdata/dsplog_day01")

    // 抽取CMCC日志中的ids
    val cmccIds = ExtractIds.extractCMCCLogIds(spark,"userprofile/testdata/cmcclog_day01")

    // 抽取doitlog中id两两对
    val doitIdPair = ExtractIDPair.extractDoitLogIDPair(spark,"userprofile/testdata/doitlog_day01")


    // 抽取DSP日志中的ids两两对
    val dspIdPair = ExtractIDPair.extractDSPLogIDPair(spark,"userprofile/testdata/dsplog_day01")

    // 抽取CMCC日志中的ids两两对
    val cmccIdPair = ExtractIDPair.extractCMCCLogIDPair(spark,"userprofile/testdata/cmcclog_day01")


    // 将3类数据的点和点、边和边union到一起
    val pair = doitIdPair.union(dspIdPair).union(cmccIdPair)
    val verticesToday = doitIds.union(dspIds).union(cmccIds)


    // 5.然后过滤掉次数<3的两两对
    val filteredPair: RDD[((Long, Long), Int)] = pair.groupBy(x => x).mapValues(_.size).filter(_._2 >= 3)

    // 6.将两两对集合映射成边集合
    val edgesToday: RDD[Edge[String]] = filteredPair.map(tp => Edge(tp._1._1, tp._1._2, ""))


    // 7.加载上一日的idm字典
    val preIdmDict = spark.read.textFile("userprofile/testdata/day01_idm_out")
    preIdmDict.cache()

    // 8.将上一日的idm字典中的各id映射成点集合
    val verticesPre: RDD[(Long, String)] = preIdmDict.rdd.flatMap(line => {
      // 取出除gid之外的各id
      val ids = line.split(":")(1).split(",")
      ids.map(str => (str.toLong, ""))
    })

    // 9.将上一日的idm字典中的各id映射成边集合
    val edgesPre = preIdmDict.rdd.flatMap(line => {
      val ids = line.split(":")(1).split(",")
      // 将第一个点和其他点连成边
      for (i <- 1 until ids.length) yield Edge(ids(0).toLong, ids(i).toLong, "")
    })

    // 10.将当天的点集合union昨天点集合， 当天边集合union昨天的边集合，构造图
    val graph = Graph(verticesToday union verticesPre, edgesToday union edgesPre)

    // 11. 求连通子图
    // (im01,g01)
    // (mac01,g01)
    // (wx01,g01)
    // (im02,g02)
    // (mac02,g02)
    // (wx02,g02)
    val flagedVertices = graph.connectedComponents().vertices

    // (g01,Iterable[(im01,g01)(mac01,g01)(wx01,g01)])
    // (g02,Iterable[(im02,g02)(mac02,g02)(wx02,g02)])
    val tmp: RDD[(VertexId, Iterable[(VertexId, VertexId)])] = flagedVertices.groupBy(_._2)

    // (g001,Set[im01,mac01,wx01,idfa01])   --> 老用户，出现了新id，导致了组id变化（这种情况，需要把组id替换成昨天的）
    // (g02,Set[im02,mac02,wx02,android02]) --> 老用户，出现了新id，但组id没变
    // (g05,Set[im05,mac05,wx05])           --> 新用户
    val tmp2: RDD[(VertexId, Set[VertexId])] = tmp.mapValues(it => it.map(_._1).toSet)


    // TODO 12.调整组id  --》  老用户，出现了新id，导致了组id变化（这种情况，需要把组id替换成昨天的）
    // TODO    整体逻辑：收集昨天的idm字典收集为一个单机集合(hashmap[id,组id])，并广播出去
    // TODO            然后，对今天的图计算结果，去map算子逐条操作（查字典：拿到字典的keyset，跟本条中的所有id求交集）


    // 将上一日的id映射字典，加工成 id->组id 的kv形式
    val predict: collection.Map[Long, Long] = preIdmDict.rdd.flatMap(line => {
      val arr = line.split(":")
      val gid = arr(0).toLong

      for (id <- arr(1).split(",")) yield (id.toLong, gid)
    }).collectAsMap()

    // 然后广播出来
    val bc = spark.sparkContext.broadcast[collection.Map[Long, Long]](predict)


    // 逐条操作今日的id映射图计算结果
    val idmResult = tmp2.map(tp => {
      // 拿出上日的字典
      val dict = bc.value

      // 取出今日的一条数据
      val todayGid = tp._1
      val todayIdSet = tp._2

      // 将今日这个组内的id们去合上日的字典中所有的key求交集
      val commonIds = dict.keySet.intersect(todayIdSet)

      // 判断 交集结果，如果为空，则说明没有交集，说明今日的这条数据是一个新用户的
      commonIds.size match {
        case 0 => (todayGid, todayIdSet)
        case _ => (dict.get(commonIds.head).get, todayIdSet) //用昨天的组id
      }

    })

    // 保存结果
    idmResult.coalesce(1)
      .map(tp=>tp._1+":"+tp._2.mkString(","))
      .saveAsTextFile("userprofile/testdata/idm_out_1")

    spark.close()

  }

}
