package cn.doitedu.profile.idmdict

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.graphx.{Edge, Graph, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset}

/**
 * @date: 2019/6/27
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  起始日的id映射字典生成程序
 */
object IdMappingInitial {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkUtils.getSparkSession("graphxdemo")
    import spark.implicits._


    // 1.读取3类日志数据，分别抽取各id标识字段
    val ds: Dataset[String] = spark.read.textFile("userprofile/testdata/day01_log/idmp_log.csv")
    ds.cache()

    // 2.把3类数据的id字段数据union成一个rdd

    // 3.将上述rdd映射成点集合
    val vertices: Dataset[(Long, String)] = ds.flatMap(line => {
      line.split(",")
    }).distinct().map(str => (str.hashCode.toLong, str))

    // 4.将上述rdd中的各类id标识，映射成两两对
    val pair: Dataset[(Long,Long)] = ds.flatMap(line=>{
      val arr = line.split(",").sorted

      for(i<-0 until arr.length-1;j <- i+1 until arr.length)
        yield (arr(i).hashCode.toLong,arr(j).hashCode.toLong)
    })


    // 5.然后过滤掉次数<3的两两对
    val filteredPair: RDD[((Long, Long), Int)] = pair.rdd.groupBy(x=>x).mapValues(_.size).filter(_._2>=3)

    // 6.将两两对集合映射成边集合
    val edges: RDD[Edge[String]] = filteredPair.map(tp=>Edge(tp._1._1,tp._1._2,""))

    // 7.将点集合和过滤后的边集合构造图模型
    val graph = Graph(vertices.rdd,edges)

    // 8.对图模型求连通子图  (某id,组id)
    // 9.从连通子图中得到分组结果
    //  (im01,g01)
    //  (mac01,g01)
    //  (wx01,g01)
    //  (im02,g02)
    //  (mac02,g02)
    //  (wx02,g02)
    val flagedVertices = graph.connectedComponents().vertices

    // 10.整理格式输出保存
    // (g01,Iterable[(im01,g01)(mac01,g01)(wx01,g01)])
    // (g02,Iterable[(im02,g02)(mac02,g02)(wx02,g02)])
    val tmp: RDD[(VertexId, Iterable[(VertexId, VertexId)])] = flagedVertices.groupBy(_._2)

    // (g01,Set[im01,mac01,wx01])
    // (g02,Set[im02,mac02,wx02])
    val tmp2 = tmp.mapValues(it=>it.map(_._1).toSet)


    // g01:im01,mac01,wx01
    // g02:im02,mac02,wx02
    val res = tmp2.map(tp=>tp._1+":"+tp._2.mkString(","))

    res.coalesce(1).saveAsTextFile("userprofile/testdata/day01_idm_out")

    spark.close()
  }


}
