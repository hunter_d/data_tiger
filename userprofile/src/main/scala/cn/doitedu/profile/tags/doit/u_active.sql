-- 统计用户行为属性标签
-- 源表：dws_traffic_agg_user
-- 需求： uid,访问次数,访问时长,访问深度

源表结构：
+--------------------------+----------------+
|         col_name         |   data_type    |
+--------------------------+----------------+
| uid                      | string         |
| session_cnts             | int            |
| access_time_amt          | bigint         |
| pv_cnts                  | int            |
| province                 | string         |
| city                     | string         |
| district                 | string         |
| county                   | string         |
| biz                      | array<string>  |
| manufacture              | string         |
| osname                   | string         |
| osver                    | string         |
| appver                   | string         |
| dt                       | string         |
|                          | NULL           |
| # Partition Information  | NULL           |
| # col_name               | data_type      |
| dt                       | string         |
+--------------------------+----------------+


-- 结果表建模：
-- ads_prf_active
drop table if exists ads_prf_active;
create table ads_prf_active(
uid string,
d_n string,
lg int,
tm bigint,
pv int
)
partitioned by (dt string)
stored as parquet
;


-- etl


insert into table ads_prf_active partition(dt='2019-06-15')

select
uid,
'd3_action' as d_n,
sum(session_cnts) as lg,
sum(access_time_amt) as tm,
sum(pv_cnts) as pv

from dws_traffic_agg_user
where dt between date_sub('2019-06-15',2) and '2019-06-15'
group by uid

union

select
uid,
'd7_action' as d_n,
sum(session_cnts) as lg,
sum(access_time_amt) as tm,
sum(pv_cnts) as pv
from dws_traffic_agg_user
where dt between date_sub('2019-06-15',6) and '2019-06-15'
group by uid

union

select
uid,
'd15_action' as d_n,
sum(session_cnts) as lg,
sum(access_time_amt) as tm,
sum(pv_cnts) as pv
from dws_traffic_agg_user
where dt between date_sub('2019-06-15',14) and '2019-06-15'
group by uid

union

select
uid,
'd30_action' as d_n,
sum(session_cnts) as lg,
sum(access_time_amt) as tm,
sum(pv_cnts) as pv
from dws_traffic_agg_user
where dt between date_sub('2019-06-15',29) and '2019-06-15'
group by uid
;



