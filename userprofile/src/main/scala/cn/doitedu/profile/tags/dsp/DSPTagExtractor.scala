package cn.doitedu.profile.tags.dsp

import cn.doitedu.common.utils.SparkUtils
import cn.doitedu.profile.tagbeans.DspTagBean
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Row}

/**
  * @date: 2019/7/2
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: DSP数据用户画像标签抽取程序
  */
object DSPTagExtractor {
  Logger.getLogger("org").setLevel(Level.WARN)

  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("dsp_tags")
    import spark.implicits._
    val dspLog = spark.read.parquet("userprofile/preout/dspout")


    val data = dspLog.select(
      "gid",
      "imei",
      "idfa",
      "openudid",
      "androidid",
      "mac",
      "adplatformproviderid",
      "appname",
      "provincename",
      "cityname",
      "district",
      "ispname",
      "networkmannername", // 联网类型
      "device", // 设备型号名称
      "biz",
      "adspacetypename", // 广告位类型名称
      "client", // 操作系统代号(1,android，2：ios，3：wp)
      "keywords"
    )


    // 打散
    val gidAndTag: DataFrame = data.flatMap({

      case Row(
      gid: String,
      imei: String,
      idfa: String,
      openudid: String,
      androidid: String,
      mac: String,
      adplatformproviderid: Int,
      appname: String,
      provincename: String,
      cityname: String,
      district: String,
      ispname: String,
      networkmannername: String,
      device: String,
      biz: String,
      adspacetypename: String,
      client: Int,
      keywords: String) => {

        val osname: String = client match {
          case 1 => "android"
          case 2 => "ios"
          case 3 => "wp"
          case _ => "other"
        }


        // 形成  （gid，标签模块，标签类别，标签值）   4元组
        Array(
          (gid, "idstags", "IME", imei),
          (gid, "idstags", "IDF", idfa),
          (gid, "idstags", "OPU", openudid),
          (gid, "idstags", "ADR", androidid),
          (gid, "idstags", "MAC", mac),
          (gid, "dspadtags", "ADP", adplatformproviderid.toString),
          (gid, "apptags", "APP", appname),
          (gid, "basetags", "PRO", provincename),
          (gid, "basetags", "CIT", cityname),
          (gid, "basetags", "DIS", district),
          (gid, "basetags", "BIZ", biz),
          (gid, "devtags", "ISP", ispname),
          (gid, "devtags", "NET", networkmannername),
          (gid, "devtags", "DEV", device),
          (gid, "dspadtags", "ADS", adspacetypename),
          (gid, "devtags", "OSN", osname),
          (gid, "kwdstags", "KWD", keywords)
        )

      }

      case _ => Array(("", "", " ", ""))
    })
      .toDF("gid", "tagmodule", "tagname", "tagvalue")

    // 将4元组，按tagvalue分组计数，得到tagvalue的分数，形成5元组
    // 5元组： gid，标签模块，标签名，标签值，分数（权重）
    val tagAndScore: DataFrame = gidAndTag
      // 过滤掉标签值为空的
      .where("gid is not null and trim(gid) != '' and  tagvalue is not null and trim(tagvalue) != '' ")
      // 统计各标签值的分数（权重）
      .groupBy("gid", "tagmodule", "tagname", "tagvalue")
      .count()

    // dsp画像标签抽取的中间结果：gid,标签模块,标签名称,标签值,分数
    tagAndScore.coalesce(1).write.parquet("userprofile/tags_out/dsp_tag_score")


    /**
      * 纯属烧脑练习
      * 将dsp标签、值、分数，按相同人聚拢到一起，封装到一个bean中
      */
    val tmp: RDD[(String, (String, String, String, Double))] = tagAndScore.rdd.map(row => {

      val gid = row.getAs[String]("gid")
      val tagname = row.getAs[String]("tagname")
      val tagmodule = row.getAs[String]("tagmodule")
      val tagvalue = row.getAs[String]("tagvalue")
      val score = row.getAs[Long]("count").toDouble

      (gid, (tagmodule, tagname, tagvalue, score))
    })

    tmp.groupByKey()
      .mapValues(iter => {
        // 按 标签模块名+标签名 分组，将标签值->分数整合到一个map中
        //  ("idstags","imei"),Map(im01->5,im02->3)
        val x: Map[(String, String), Map[String, Double]] = iter.groupBy(tp => (tp._1, tp._2)).mapValues(iter => iter.map(tp => (tp._3, tp._4)).toMap)

        //  (idstags,(imei,Map(im01->5,im02->3))
        val tagsRes: Map[String, Map[String, Map[String, Double]]] = x.toList.map(tp => (tp._1._1, (tp._1._2, tp._2)))
          .groupBy(_._1)
          .map(tp => (tp._1, tp._2.map(tp => tp._2).toMap))

        tagsRes
      })
      // (gid,Map[String,(String,Map[String,Double]]))
      .map(tp => {
      val empty = Map.empty[String, Map[String, Double]]

      DspTagBean(
        tp._1,
        if (tp._2.get("idstags").isDefined) tp._2.get("idstags").get else empty,
        if (tp._2.get("basetags").isDefined) tp._2.get("basetags").get else empty,
        if (tp._2.get("dspadtags").isDefined) tp._2.get("dspadtags").get else empty,
        if (tp._2.get("devtags").isDefined) tp._2.get("devtags").get else empty,
        if (tp._2.get("apptags").isDefined) tp._2.get("apptags").get else empty,
        if (tp._2.get("kwdstags").isDefined) tp._2.get("kwdstags").get else empty
      )
    })
      .toDF()
      .show(10, false)


    spark.close()


  }

}
