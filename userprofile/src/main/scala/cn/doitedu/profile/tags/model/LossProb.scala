package cn.doitedu.profile.tags.model

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.classification.NaiveBayes
import org.apache.spark.ml.feature.MinMaxScaler
import org.apache.spark.ml.linalg.Vectors

import scala.collection.mutable

/**
 * @date: 2019/7/20
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  流失率 模型标签计算
  *              算法：朴素贝叶斯
  *              特征：消费行为属性、访问行为属性、退换货行为、评论行为、年龄、性别、收入水平、职业........
 */
object LossProb {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.ERROR)
    val spark = SparkUtils.getSparkSession("loss_prob")

    import spark.implicits._

    // 加载提取好的用户特征数据
    val df = spark.read.option("header","true").csv("userprofile/testdata/loss_sample/input")

    // 特征值向量化
    val data = df.selectExpr("label","gid","array(cast(3_cs as double),cast(15_cs as double),cast(3_xf as double),cast(15_xf as double),cast(3_th as double),cast(15_th as double),cast(3_hp as double),cast(15_hp as double),cast(3_cp as double),cast(15_cp as double),cast(last_dl as double),cast(last_xf as double)) as arr")
        .map(row=>{

          val label = row.getAs[String]("label").toDouble
          val gid = row.getAs[String]("gid")
          val arr = row.getAs[mutable.WrappedArray[Double]]("arr")

          val vec = Vectors.dense(arr.toArray)

          (label,gid,vec)
        }).toDF("label","gid","vec")


    // 特征值归一化
    val scaler = new MinMaxScaler().setInputCol("vec").setOutputCol("norm")
    val norm = scaler.fit(data).transform(data)


    // 构建naive bayes 算法模型
    val bayes = new NaiveBayes()
        .setLabelCol("label")   //分类标签列
        .setFeaturesCol("norm")   // 特征向量列
        .setSmoothing(0.1)     // 平滑稀疏
       .setProbabilityCol("prob")   // 算出来的概率列
      .setPredictionCol("prediction")

    // 训练模型
    val model = bayes.fit(norm)

    //model.save("")   训练一次模型，保存好，可以使用多次
    //NaiveBayes.load("")

    // 用模型预测
    // 把需要预测的数据加载，并向量化
    // 加载提取好的用户特征数据
    val df2 = spark.read.option("header","true").csv("userprofile/testdata/loss_predict/input")

    // 特征值向量化
    val data2 = df2.selectExpr("gid","array(cast(3_cs as double),cast(15_cs as double),cast(3_xf as double),cast(15_xf as double),cast(3_th as double),cast(15_th as double),cast(3_hp as double),cast(15_hp as double),cast(3_cp as double),cast(15_cp as double),cast(last_dl as double),cast(last_xf as double)) as arr")
      .map(row=>{

        val gid = row.getAs[String]("gid")
        val arr = row.getAs[mutable.WrappedArray[Double]]("arr")

        val vec = Vectors.dense(arr.toArray)

        (gid,vec)
      }).toDF("gid","vec")


    // 特征值归一化
    val scaler2 = new MinMaxScaler().setInputCol("vec").setOutputCol("norm")
    val norm2 = scaler2.fit(data2).transform(data2)

    val result = model.transform(norm2).selectExpr("gid","prob")


    result.show(10,false)
    result.write.parquet("userprofile/testdata/loss_predict/output")

    spark.close()

  }


}
