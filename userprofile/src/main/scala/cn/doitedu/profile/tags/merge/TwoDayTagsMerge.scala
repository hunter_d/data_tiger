package cn.doitedu.profile.tags.merge

import java.io.{BufferedReader, InputStreamReader}
import java.net.URI

import cn.doitedu.common.utils.SparkUtils
import cn.doitedu.profile.utils.TagRDD2JsonRDD
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.sql.Row

import scala.collection.mutable
import scala.io.Source

object TwoDayTagsMerge {

  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("merge")
    import spark.implicits._


    /*val conf = new Configuration()
    val fs = FileSystem.get(new URI("hdfs://c701:8020"),conf,"root")
    val in = fs.open(new Path("/dict/tag_decay_ratio.dict"))
    val br = new BufferedReader(new InputStreamReader(in))
    var line:String = null
    var flag = true
    val dict = new mutable.HashMap[String,Double]()
    while(flag){
      line = br.readLine()
      if(line != null) {
        val arr = line.split(",")
        dict.put(arr(0)+"_"+arr(1),arr(2).toDouble)
      }else{
        flag = false
      }
    }*/

    // 加载衰减系数字典
    val lines: Iterator[String] = Source.fromFile("userprofile/src/main/resources/tag_decay_ratio.dict").getLines()
    val tpLst = for (line <- lines) yield {
      var arr = line.split(",")
      (arr(0) + "_" + arr(1), arr(2).toDouble)
    }
    val dict = tpLst.toMap
    val bc = spark.sparkContext.broadcast(dict)


    // 加载两日的标签数据
    val yesterday = spark.read.parquet("userprofile/tags_out/union_tag_score")
    val today = spark.read.option("header", "true").csv("userprofile/tags_out/union_tag_score_2")
      .selectExpr("gid", "tagmodule", "tagname", "tagvalue", "cast(score as double) as score")


    // merge
    yesterday.createTempView("yesterday")
    today.createTempView("today")

    val tupleResult = spark.sql(
      """
        |select
        |a.gid as agid,
        |a.tagmodule as atagmodule,
        |a.tagname as atagname,
        |a.tagvalue as atagvalue,
        |a.score as ascore,
        |b.*
        |from yesterday a  full join today b
        |on a.gid=b.gid and a.tagmodule=b.tagmodule and a.tagname=b.tagname and a.tagvalue=b.tagvalue
        |
      """.stripMargin)
      .rdd
      .map(row => {

        // 衰减系数字典
        val decayRatio = bc.value

        val agid = row.getAs[String]("agid")
        val atagmodule = row.getAs[String]("atagmodule")
        val atagname = row.getAs[String]("atagname")
        val atagvalue = row.getAs[String]("atagvalue")
        val ascore = row.getAs[Double]("ascore")

        val bgid = row.getAs[String]("gid")
        val btagmodule = row.getAs[String]("tagmodule")
        val btagname = row.getAs[String]("tagname")
        val btagvalue = row.getAs[String]("tagvalue")
        val bscore = row.getAs[Double]("score")


        var gid: String = agid
        var tagmodule: String = atagmodule
        var tagname: String = atagname
        var tagvalue: String = atagvalue
        var score: Double = ascore

        // +----+----------+--------+---------+------+----+---------+-------+---------+-----+
        //|agid|atagmodule|atagname|atagvalue|ascore|gid |tagmodule|tagname|tagvalue |score|
        //+----+----------+--------+---------+------+----+---------+-------+---------+-----+
        // |2234|activetags|TML     |d3       |19.0  |null|null     |null   |null     |null |
        if (agid != null && bgid == null) {
          val ratio = decayRatio.getOrElse(atagmodule + "_" + atagname, 0.9d)
          score = ascore * ratio
        }

        //+----+----------+--------+---------+------+----+---------+-------+---------+-----+
        //|agid|atagmodule|atagname|atagvalue|ascore|gid |tagmodule|tagname|tagvalue |score|
        //+----+----------+--------+---------+------+----+---------+-------+---------+-----+
        //|1234|apptags   |DES     |视频       |24.0  |1234|apptags  |DES    |视频       |24.0 |
        if(agid != null && bgid != null){
          score = ascore+bscore
        }


        //+----+----------+--------+---------+------+----+---------+-------+---------+-----+
        //|agid|atagmodule|atagname|atagvalue|ascore|gid |tagmodule|tagname|tagvalue |score|
        //+----+----------+--------+---------+------+----+---------+-------+---------+-----+
        //|null|null      |null    |null     |null  |3234|basetags |PRO    |日本省      |14.0 |
        if(agid == null && bgid != null){
          gid = bgid
          tagmodule = btagmodule
          tagname = btagname
          tagvalue = btagvalue
          score = bscore
        }

        (gid, tagmodule, tagname, tagvalue, score)
      })
        .toDF("gid","tagmodule","tagname","tagvalue","score")

    val jsonResult = TagRDD2JsonRDD.tag2Json(tupleResult.rdd)
    jsonResult.take(10).foreach(println)




    spark.close()
  }
}
