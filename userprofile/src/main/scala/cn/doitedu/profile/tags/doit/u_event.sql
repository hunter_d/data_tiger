-- 统计用户事件行为属性标签
-- 源表：dws_event_overall_agg_u
-- 需求： uid,rate次数,favor次数,thumbup次数

+--------------------------+------------+----------+
|         col_name         | data_type  | comment  |
+--------------------------+------------+----------+
| event_type               | string     |          |
| uid                      | string     |          |
| cnts                     | int        |          |
| dt                       | string     |          |
|                          | NULL       | NULL     |
| # Partition Information  | NULL       | NULL     |
| # col_name               | data_type  | comment  |
| dt                       | string     |          |
+--------------------------+------------+----------+


-- 结果表建模
create table ads_prf_event(
uid string,d_n string,event_type string, cnts int
)
partitioned by (dt string)
stored as parquet;



-- ETL
insert into table ads_prf_event partition(dt='2019-06-15')

select
uid,
'd3' as d_n,
event_type,
sum(cnts) as cnts
from dws_event_overall_agg_u
where dt between date_sub('2019-06-15',2) and '2019-06-15'
group by uid,event_type

union

select
uid,
'd7' as d_n,
event_type,
sum(cnts) as cnts
from dws_event_overall_agg_u
where dt between date_sub('2019-06-15',6) and '2019-06-15'
group by uid,event_type


union

select
uid,
'd15' as d_n,
event_type,
sum(cnts) as cnts
from dws_event_overall_agg_u
where dt between date_sub('2019-06-15',14) and '2019-06-15'
group by uid,event_type


union

select
uid,
'd30' as d_n,
event_type,
sum(cnts) as cnts
from dws_event_overall_agg_u
where dt between date_sub('2019-06-15',29) and '2019-06-15'
group by uid,event_type
;
