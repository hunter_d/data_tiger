package cn.doitedu.profile.tags.combine

import java.util

import cn.doitedu.common.utils.SparkUtils
import com.alibaba.fastjson.JSON
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{DataTypes, StructType}

import scala.beans.BeanProperty
import scala.collection.{JavaConverters, immutable}

object TagsCombine {

  Logger.getLogger("org").setLevel(Level.WARN)

  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("")

    import org.apache.spark.sql.functions._
    import spark.implicits._

    /*val cmccTags = spark.read.parquet("userprofile/tags_out/cmcc_tag_score")
    val doitTags = spark.read.parquet("userprofile/tags_out/doit_tag_score")
    val dspTags = spark.read.parquet("userprofile/tags_out/dsp_tag_score")

    val unionTags = cmccTags.union(doitTags).union(dspTags)
        .show(10,false)*/


    val schema = new StructType()
      .add("gid", DataTypes.StringType)
      .add("tagmodule", DataTypes.StringType)
      .add("tagname", DataTypes.StringType)
      .add("tagvalue", DataTypes.StringType)
      .add("score", DataTypes.DoubleType)

    val unionTags = spark.read.option("header", "true").schema(schema).csv("userprofile/tags_out/testtags/")
    /*unionTags.printSchema()
    unionTags.show(10,false)*/


    // 聚合分数
    val aggedTags = unionTags.groupBy("gid", "tagmodule", "tagname", "tagvalue").agg(sum('score).as("score"))
    aggedTags.coalesce(1).write.parquet("userprofile/tags_out/union_tag_score")


    spark.close()


  }

}
