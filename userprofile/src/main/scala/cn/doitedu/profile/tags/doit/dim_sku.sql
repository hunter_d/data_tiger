-- 商品详情宽表构建
-- 源表： ods_sku_info  ods_base_category3 ods_base_category2 ods_base_category1
0: jdbc:hive2://localhost:10000> desc ods_sku_info;
+--------------------------+------------+
|         col_name         | data_type  |
+--------------------------+------------+
| id                       | bigint     |
| spu_id                   | bigint     |
| price                    | double     |
| sku_name                 | string     |
| sku_desc                 | string     |
| weight                   | double     |
| tm_id                    | bigint     |
| category3_id             | bigint     |
| sku_default_img          | string     |
| create_time              | string     |
| dt                       | string     |
|                          | NULL       |
| # Partition Information  | NULL       |
| # col_name               | data_type  |
| dt                       | string     |
+--------------------------+------------+

0: jdbc:hive2://localhost:10000> desc ods_base_category3;
+--------------------------+------------+
|         col_name         | data_type  |
+--------------------------+------------+
| id                       | bigint     |
| name                     | string     |
| category2_id             | bigint     |
| dt                       | string     |
|                          | NULL       |
| # Partition Information  | NULL       |
| # col_name               | data_type  |
| dt                       | string     |
+--------------------------+------------+

-- 结果表建模：dim_sku

create table dim_sku(
skuid bigint,
spu_id bigint,
price double,
sku_name string,
sku_desc string,
weight double ,
tm_id  bigint ,
category3_id bigint,
category3_name string,
category2_id bigint,
category2_name string,
category1_id bigint,
category1_name string
)
stored as parquet;

-- ETL
insert into table dim_sku
select
a.id,
a.spu_id,
a.price,
a.sku_name,
a.sku_desc,
a.weight,
a.tm_id,
c3.id,
c3.name,
c2.id,
c2.name,
c1.id,
c1.name
from ods_sku_info a
join ods_base_category3 c3 on a.category3_id = c3.id
join ods_base_category2 c2 on c3.category2_id = c2.id
join ods_base_category1 c1 on c2.category1_id = c1.id
;
