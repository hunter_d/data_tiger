package cn.doitedu.profile.tags.doit

import cn.doitedu.common.utils.{DictLoader, SparkUtils}
import com.hankcs.hanlp.HanLP
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row

/**
  * @date: 2019/7/2
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 多易内部数据标签抽取程序
  */
object DoitTagExtractor {
  Logger.getLogger("org").setLevel(Level.WARN)

  def main(args: Array[String]): Unit = {

    // 1. 从原始日志中抽取  id标识信息、设备信息、区域信息、商品信息
    val spark = SparkUtils.getSparkSession("doit_tags")
    import spark.implicits._
    val doitLog = spark.read.parquet("dataware/data/output-pre")
    doitLog.show(10, false)


    // 加载idmp字典
    val id_gid = DictLoader.loadIDM(spark, "userprofile/testdata/idm_out_1")
    val idm: collection.Map[String, String] = id_gid.collectAsMap()
    val bc = spark.sparkContext.broadcast(idm)


    // 加载商品品类信息
    val skuinfo = spark.read.parquet("hdfs://c701:8020/user/hive/warehouse/tiger.db/dim_sku")
    val skubc: collection.Map[Long, List[String]] = skuinfo.select("skuid", "c3name", "c2name", "c1name", "sku_name", "sku_desc")
      //.selectExpr("map(skuid,array(c3name,c2name,c1name,sku_name,sku_desc))")
      .rdd
      .map({
        case Row(skuid: Long, c3name: String, c2name: String, c1name: String, sku_name: String, sku_desc: String) =>
          (skuid, List(c1name, c2name, c3name, sku_name, sku_desc))

        case _ => (-1L, List.empty[String])
      }).collectAsMap()
    val bcSku = spark.sparkContext.broadcast(skubc)


    // 抽取日志中画像所需要的各个字段
    val data = doitLog.selectExpr(
      "account",
      "imei",
      "androidId",
      "deviceId",
      "osName",
      "manufacture", "carrier", "netType", "province", "city", "district", "eventMap['skuid'] as skuid")

    // 集成gid
    val fourTupleRdd = data.rdd.flatMap(row => {
      val account = row.getAs[String]("account")
      val imei = row.getAs[String]("imei")
      val androidId = row.getAs[String]("androidId")
      val deviceId = row.getAs[String]("deviceId")

      val osName = row.getAs[String]("osName")
      val manufacture = row.getAs[String]("manufacture")
      val carrier = row.getAs[String]("carrier")
      val netType = row.getAs[String]("netType")
      val province = row.getAs[String]("province")
      val city = row.getAs[String]("city")
      val district = row.getAs[String]("district")
      val skuid = row.getAs[String]("skuid")

      // 取 gid
      val notblankId = Array(account, imei, androidId, deviceId).filter(id => StringUtils.isNotBlank(id))(0)
      val idmDict = bc.value
      var gidOption = idmDict.get(notblankId.hashCode.toLong + "")
      var gid = ""
      if (gidOption.isDefined) gid = gidOption.get


      // 形成  （gid，标签模块，标签类别，标签值）   4元组
      Array(
        (gid, "idstags", "IME", imei),
        (gid, "idstags", "ADR", androidId),
        (gid, "idstags", "DID", deviceId),
        (gid, "basetags", "PRO", province),
        (gid, "basetags", "CIT", city),
        (gid, "basetags", "DIS", district),
        (gid, "devtags", "ISP", carrier),
        (gid, "devtags", "NET", netType),
        (gid, "devtags", "DEV", manufacture),
        (gid, "devtags", "OSN", osName),
        (gid, "skuid", "SKUID", skuid) // 这一条不是真正的标签，只是一个过渡状态
      )
    })
      // 过滤掉标签值空的
      .filter(tp => StringUtils.isNotBlank(tp._4))

    // 再为skuid关联 商品的一级品类，2级品类，3级品类，商品名称
    val skuedRdd = fourTupleRdd.flatMap(tp => {
      // 获取商品信息字典
      val skuDict = bcSku.value

      // 判断，如果一条标签数据的模块类型为skuid，那么，这条数据还需要去关联商品信息，形成多种标签
      if (tp._2.equals("skuid")) {
        val skuinfoOption = skuDict.get(tp._4.toLong)
        if (skuinfoOption.isDefined) {
          val info = skuinfoOption.get
          val title = info(3)
          val desc = info(4)

          import scala.collection.JavaConversions._
          val keywords = HanLP.segment(title + " " + desc).map(term => term.word).filter(_.size > 1)


          val kwLst = (for (w <- keywords) yield (tp._1, "itemtags", "KWD", w)).toList
          val catLst = List(
            (tp._1, "itemtags", "CT1", info(0)),
            (tp._1, "itemtags", "CT2", info(1)),
            (tp._1, "itemtags", "CT3", info(2))
          )

          kwLst ++ catLst

        } else {
          List.empty[(String, String, String, String)]
        }
      } else {
        List(tp)
      }
    })

    // 阶段1结果：将4元组RDD，找tagvalue来计数，得到tagvalue的分数
    // 形成了一个5元组: gid,标签模块,标签名,标签值,分数（权重）
    val logTags = skuedRdd.toDF("gid", "tagmodule", "tagname", "tagvalue")
      .groupBy("gid", "tagmodule", "tagname", "tagvalue")
      .count()


    // 阶段2. 从数仓ads层抽取  画像活跃报表  画像事件报表   画像消费报表
    //  ads_prf_active
    //  +---------+-------------+-------+--------+-------+-------------+
    //|  t.uid  |    t.d_n    | t.lg  |  t.tm  | t.pv  |    t.dt     |
    //+---------+-------------+-------+--------+-------+-------------+
    //| 00175   | d15_action  | 1     | 2226   | 1     | 2019-06-15  |
    //| 00175   | d30_action  | 1     | 2226   | 1     | 2019-06-15  |
    //| 00175   | d3_action   | 1     | 2226   | 1     | 2019-06-15  |
    //| 00175   | d7_action   | 1     | 2226   | 1     | 2019-06-15  |
    //+---------+-------------+-------+--------+-------+-------------+|
    // 设计：
    // 模块：active
    //         lg:
    //            d15:1
    //            d3:1
    //         tm:
    //            d15:2226
    //            d3: 1280


    /**
      * 抽取用户活跃属性标签
      * 注意：这块数据，是不用再去计算tagvalue的分数的，数仓中已有统计结果
      */
    val active = spark.read.parquet("hdfs://c701:8020/user/hive/warehouse/tiger.db/ads_prf_active")

    // 5元组
    val activeTags = active.rdd.flatMap(row => {
      val uid = row.getAs[String]("uid")
      var d_n = row.getAs[String]("d_n")
      d_n = d_n.substring(0, d_n.indexOf("_"))

      val lg = row.getAs[Int]("lg")
      val tm = row.getAs[Long]("tm")
      val pv = row.getAs[Int]("pv")


      // 查询gid
      val idmDict = bc.value
      val gidOption = idmDict.get(uid.hashCode.toLong + "")
      var gid = ""
      if (gidOption.isDefined) gid = gidOption.get

      // 拼装5元组
      List(
        (gid, "activetags", "LGI", d_n, lg.toDouble),
        (gid, "activetags", "TML", d_n, tm.toDouble),
        (gid, "activetags", "PVS", d_n, pv.toDouble)
      )
    })
      .toDF("gid", "tagmodule", "tagname", "tagvalue", "score")




    //  ads_prf_event
    //+--------+--------+---------------+---------+-------------+
    //| t.uid  | t.d_n  | t.event_type  | t.cnts  |    t.dt     |
    //+--------+--------+---------------+---------+-------------+
    //| 00175  | d15    | ad_show       | 2       | 2019-06-15  |
    //| 00175  | d15    | pg_view       | 1       | 2019-06-15  |
    //| 00175  | d15    | search        | 1       | 2019-06-15  |
    //| 00175  | d15    | startup       | 1       | 2019-06-15  |
    //| 00175  | d3     | ad_show       | 2       | 2019-06-15  |
    //+--------+--------+---------------+---------+-------------+
    /**
      * 抽取用户事件行为属性标签
      * 注意：这块数据，是不用再去计算tagvalue的分数的，数仓中已有统计结果
      */
    val eventTags = spark.read.parquet("hdfs://c701:8020/user/hive/warehouse/tiger.db/ads_prf_event")
      .rdd
      .map(row => {
        val uid = row.getAs[String]("uid")
        var d_n = row.getAs[String]("d_n")
        val event_type = row.getAs[String]("event_type")
        val cnts = row.getAs[Int]("cnts")

        // 查询gid
        val idmDict = bc.value
        val gidOption = idmDict.get(uid.hashCode.toLong + "")
        var gid = ""
        if (gidOption.isDefined) gid = gidOption.get

        (gid, "eventtags", event_type, d_n, cnts)
      })
      .toDF("gid", "tagmodule", "tagname", "tagvalue", "score")

    logTags.union(activeTags).union(eventTags).coalesce(1).write.parquet("userprofile/tags_out/doit_tag_score")

    spark.close()
  }


}
