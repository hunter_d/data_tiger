package cn.doitedu.profile.tags.cmcc

import cn.doitedu.common.utils.SparkUtils
import com.hankcs.hanlp.HanLP
import org.apache.spark.sql.Dataset

/**
  * @date: 2019/7/3
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: CMCC数据标签抽取程序
  */
object CMCCTagExtractor {

  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("cmcc_tags")
    import spark.implicits._

    val cmcc = spark.read.parquet("userprofile/preout/cmccout")

    val tuple4DF = cmcc.rdd.flatMap(row => {
      val gid = row.getAs[String]("gid")

      val phone = row.getAs[String]("phone")
      val imei = row.getAs[String]("imei")
      val imsi = row.getAs[String]("imsi")
      val cat1 = row.getAs[String]("cat1")
      val cat2 = row.getAs[String]("cat2")
      val cat3 = row.getAs[String]("cat3")
      val brand = row.getAs[String]("brand")

      val title = row.getAs[String]("title")

      import scala.collection.JavaConversions._
      val words = HanLP.segment(title).map(term => term.word).filter(_.size > 1).toList
      val kwLst = for (w <- words) yield (gid, "itemtags", "KWD", w)


      val itemLst = List(
        (gid, "idstags", "PHO", phone),
        (gid, "idstags", TagNames.IMEI, imei),
        (gid, "idstags", TagNames.IMSI, imsi),
        (gid, "itemtags", "CT1", cat1),
        (gid, "itemtags", "CT2", cat2),
        (gid, "itemtags", "CT3", cat3),
        (gid, "itemtags", "BRD", brand)
      )
      kwLst ++ itemLst
    }).toDF("gid","tagmodule","tagname","tagvalue")

    val tuple5DF = tuple4DF.groupBy("gid","tagmodule","tagname","tagvalue").count()

    tuple5DF.show(20,false)

    tuple5DF.write.parquet("userprofile/tags_out/cmcc_tag_score")


    spark.close()

  }

}
