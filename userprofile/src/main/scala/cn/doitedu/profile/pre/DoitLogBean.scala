package cn.doitedu.profile.pre

/**
 * @date: 2019/6/30
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  用户画像中用于封装doit流量日志的bean
 */
case class DoitLogBean (
                    var gid:String,
                    var cookieid :String,
                    var account :String,
                    var imei :String,
                    var osName :String,
                    var osVer :String,
                    var resolution :String,
                    var androidId :String,
                    var manufacture :String,
                    var deviceId :String,
                    var appid :String,
                    var appVer :String,
                    var release_ch :String,
                    var promotion_ch :String,
                    var areacode :String,
                    var longtitude :Double,
                    var latitude :Double,
                    var carrier :String,
                    var netType :String,
                    var cid_sn :String,
                    var ip :String,
                    var sessionId :String,
                    var logType :String,
                    var commit_time:Long,
                    var province:String,
                    var city:String,
                    var district:String,
                    var county:String,
                    var biz:List[String],
                    var eventMap: Map[String, String]
                  )
