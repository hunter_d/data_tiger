package cn.doitedu.profile.pre

import ch.hsr.geohash.GeoHash
import cn.doitedu.common.beans.LogBean
import cn.doitedu.common.utils.{JsonParse, SparkUtils}
import com.hankcs.hanlp.HanLP
import org.apache.commons.lang3.StringUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, Row}

/**
  * @date: 2019/6/30
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 公司业务系统用户访问行为日志（流量日志）
  */
object DoitLogPreprocess {

  def main(args: Array[String]): Unit = {

    // 解析json
    val spark = SparkUtils.getSparkSession("doitlogpreprocess")
    import spark.implicits._


    // 加载停止词典
    val stop: Dataset[String] = spark.read.textFile("userprofile/dicts/stopwords")
    val stopwords = stop.collect.toSet
    val bc = spark.sparkContext.broadcast(stopwords)

    // 加载商圈字典
    val biz = spark.read.parquet("userprofile/dicts/biz")
    val bizmap = biz.map({
      case Row(geo: String, province: String, city: String, district: String, biz: String) => (geo, List(province, city, district, biz))
    }).rdd.collectAsMap()
    val bcBiz = spark.sparkContext.broadcast[collection.Map[String, List[String]]](bizmap)


    // 加载区域字典
    val area = spark.read.parquet("userprofile/dicts/area")
    val areamap: collection.Map[String, List[String]] = area.map({
      case Row(geo: String, province: String, city: String, district: String, county: String) => (geo, List(province, city, district))
    }).rdd.collectAsMap()
    val bcArea = spark.sparkContext.broadcast[collection.Map[String, List[String]]](areamap)


    // 加载id映射字典
    val idm = spark.read.textFile("userprofile/testdata/idm_out_1")
    val idmmap: collection.Map[String, String] = idm.rdd.flatMap(line => {
      val arr = line.split(":")
      val gid = arr(0)
      val idsList = arr(1).split(",", -1).toList
      for (id <- idsList) yield (id, gid)
    }).collectAsMap()
    val bcIDM = spark.sparkContext.broadcast[collection.Map[String, String]](idmmap)


    // 加载流量日志
    val logDS = spark.read.textFile("userprofile/testdata/doitlog_day01/")

    val beanRDD: RDD[LogBean] = JsonParse.parseJson(logDS)


    // 清洗过滤
    beanRDD
      .filter(x => x != null)
      // id | account | sessionid | imei
      .filter(bean =>
      !(StringUtils.isBlank(bean.account) && StringUtils.isBlank(bean.sessionId) && StringUtils.isBlank(bean.imei))
    )

      // 页面标题的分词处理
      .map(bean => {
      val stwDict: Set[String] = bc.value
      val b = DoitLogBean(
        "",
        bean.cookieid,
        bean.account,
        bean.imei,
        bean.osName,
        bean.osVer,
        bean.resolution,
        bean.androidId,
        bean.manufacture,
        bean.deviceId,
        bean.appid,
        bean.appVer,
        bean.release_ch,
        bean.promotion_ch,
        bean.areacode,
        bean.longtitude,
        bean.latitude,
        bean.carrier,
        bean.netType,
        bean.cid_sn,
        bean.ip,
        bean.sessionId,
        bean.logType,
        bean.commit_time,
        "",
        "",
        "",
        "",
        Nil,
        bean.eventMap
      )


      val titleOption = bean.eventMap.get("title")
      if (titleOption.isDefined && StringUtils.isNotBlank(titleOption.get)) {
        // 做分词 ：中文分词是一件很复杂的事情，有一些简单分词算法包：比如 CJKAnalyzer
        // 也有一些高级的分词算法包：比如相当流行的 IKAnalyzer， 庖丁分词器 , Hanlp  NLP工具包
        import scala.collection.JavaConversions._
        val title_words = HanLP.segment(titleOption.get).map(term => term.word).filter(_.size > 1).filter(!stwDict.contains(_)).mkString(" ")

        val event: Map[String, String] = bean.eventMap + ("title" -> title_words)
        b.eventMap = event
      }
      // 返回结果
      b
    })
      // 集成省市区商圈信息
      .map(b => {

      // 获取字典
      val bizDict = bcBiz.value
      val areaDict = bcArea.value


      val lat: Double = b.latitude
      val lng: Double = b.longtitude

      // 判断经纬度的范围
      var geo = ""
      if (lng > -180 && lng < 180 && lat < 90 && lat > -90) {
        geo = GeoHash.withCharacterPrecision(lat, lng, 6).toBase32
      }

      var province = ""
      var city = ""
      var district = ""
      var biz: List[String] = Nil

      // 匹配行政区域和商圈信息
      val option: Option[List[String]] = bizDict.get(geo)
      if (option.isDefined) {
        val lst = option.get
        province = lst(0)
        city = lst(1)
        district = lst(2)
        biz = lst(3) :: Nil
      } else {
        val op = areaDict.get(geo)
        if (op.isDefined) {
          val lst = op.get
          province = lst(0)
          city = lst(1)
          district = lst(2)
        }
      }

      b.province = province
      b.city = city
      b.district = district
      b.biz = biz
      b
    })
      // 集成图id
      .map(bean => {

      val idmDict = bcIDM.value

      val account = bean.account
      val imei = bean.imei
      val androidId = bean.androidId
      val deviceId = bean.deviceId


      // 集成统一图id
      val ids = Array(account, imei, androidId, deviceId)
      var flag = true
      var gid = ""
      for (id <- ids if flag) {
        val gidOption = idmDict.get(id.hashCode.toLong + "")
        if (gidOption.isDefined) {
          gid = gidOption.get
          flag = false
        }
      }
      bean.gid = gid
      bean
    })
      .toDF()

      .coalesce(1)

      .write./*partitionBy("province").*/parquet("userprofile/preout/doitout")


    spark.close()

  }


}
