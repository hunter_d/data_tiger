package cn.doitedu.profile.pre

import ch.hsr.geohash.GeoHash
import cn.doitedu.common.utils.SparkUtils
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.Row

/**
  * @date: 2019/6/29
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: DSP日志预处理程序
  */
object DSPLogPreprocess {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkUtils.getSparkSession("dsp_pre")
    import spark.implicits._

    // 加载区域信息字典
    /*
    +------+--------+----+--------+--------+
    |geo   |province|city|district|county  |
    +------+--------+----+--------+--------+
    |wx4g15|北京市     |北京市 |东城区     |东华门街道办事处|
    |wx4g1j|北京市     |北京市 |东城区     |景山街道办事处 |
     */
    val area = spark.read.parquet("userprofile/dicts/area")
    val areamap: collection.Map[String, List[String]] = area.map({
      case Row(geo: String, province: String, city: String, district: String, county: String) => (geo, List(province, city, district))
    }).rdd.collectAsMap()
    val bcArea = spark.sparkContext.broadcast[collection.Map[String, List[String]]](areamap)


    // 加载商圈字典
    /*
    +------+--------+----+--------+---+
    |geo   |province|city|district|biz|
    +------+--------+----+--------+---+
    |wx4g11|北京市     |北京市 |东城区     |王府井|
    |wx4g14|北京市     |北京市 |东城区     |灯市口|
     */
    val biz = spark.read.parquet("userprofile/dicts/biz")
    val bizmap = biz.map({
      case Row(geo: String, province: String, city: String, district: String, biz: String) => (geo, List(province, city, district, biz))
    }).rdd.collectAsMap()
    val bcBiz = spark.sparkContext.broadcast[collection.Map[String, List[String]]](bizmap)


    // 加载app信息字典
    val appinfo = spark.read.textFile("userprofile/dicts/appinfo")
    val appmap: collection.Map[String, List[String]] = appinfo.rdd.map(line => {
      val arr = line.split("\t", -1)
      (arr(4), List(arr(1), arr(5)))
    }).collectAsMap()
    val bcApp = spark.sparkContext.broadcast[collection.Map[String, List[String]]](appmap)


    // 加载id映射字典
    val idm = spark.read.textFile("userprofile/testdata/idm_out_1")
    val idmmap: collection.Map[String, String] = idm.rdd.flatMap(line => {
      val arr = line.split(":")
      val gid = arr(0)
      val idsList = arr(1).split(",", -1).toList
      for (id <- idsList) yield (id, gid)
    }).collectAsMap()
    val bcIDM = spark.sparkContext.broadcast[collection.Map[String, String]](idmmap)


    // 加载dsp日志
    val dsplog = spark.read.textFile("userprofile/testdata/dsplog_day01")

    val res: RDD[DspLogBean] = dsplog.rdd
      // 数据解析
      .map(line => line.split(",", -1))
      // 数据清洗
      .filter(_.size >= 85)
      .filter(arr => {
        StringUtils.isNotBlank(
          (if (arr(46) == null) "" else arr(46)) +
            (if (arr(47) == null) "" else arr(47)) +
            (if (arr(48) == null) "" else arr(48)) +
            (if (arr(49) == null) "" else arr(49)) +
            (if (arr(50) == null) "" else arr(50))
        )
      })
      // 数据集成
      .mapPartitions(iter => {
      // 拿各种字典
      val areaDict = bcArea.value
      val bizDict = bcBiz.value
      val appDict = bcApp.value
      val idmDict = bcIDM.value

      iter.map(arr => {

        // 集成省市区及商圈信息
        val lng = if(StringUtils.isNumeric(arr(22))) arr(22).toDouble else -300
        val lat = if(StringUtils.isNumeric(arr(23))) arr(23).toDouble else -300

        // 判断经纬度的范围
        var geo = ""
        if(lng > -180 && lng < 180 && lat < 90 && lat > -90 ) {
          geo = GeoHash.withCharacterPrecision(lat, lng, 6).toBase32
        }

        var province = arr(24)
        var city = arr(25)
        var district = arr(37)
        var biz = ""

        // 匹配行政区域和商圈信息
        val option: Option[List[String]] = bizDict.get(geo)
        if (option.isDefined) {
          val lst = option.get
          province = lst(0)
          city = lst(1)
          district = lst(2)
          biz = lst(3)
        } else {
          val op = areaDict.get(geo)
          if (op.isDefined) {
            val lst = op.get
            province = lst(0)
            city = lst(1)
            district = lst(2)
          }
        }

        // 集成app信息
        val appid = arr(13)
        var appname = arr(14)
        var appdesc = ""

        val appoption = appDict.get(appid)
        if (appoption.isDefined) {
          val appinfoLst = appoption.get
          appname = appinfoLst(0)
          appdesc = appinfoLst(1)
        }

        // 集成统一图id
        val ids = Array(arr(46), arr(47), arr(48), arr(49), arr(50))
        var flag = true
        var gid = ""
        for (id <- ids if flag) {
          val gidOption = idmDict.get(id.hashCode.toLong + "")
          if (gidOption.isDefined) {
            gid = gidOption.get
            flag = false
          }
        }

        // 封装成一个caseclass
        val bean = DspLogBean.genDspLogBean(arr)


        bean.provincename = province
        bean.cityname = city
        bean.district = district
        bean.biz = biz

        bean.appDesc = appdesc
        bean.appname = appname

        bean.gid = gid

        bean
      })
    }

    )

    // 输出结果为parquet文件
    res.toDF().write.parquet("userprofile/preout/dspout")

    spark.close()

  }


}
