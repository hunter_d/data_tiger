package cn.doitedu.profile.pre

import cn.doitedu.common.utils.{DictLoader, SparkUtils}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD

object CmccLogPreprocess {
  Logger.getLogger("org").setLevel(Level.WARN)
  def main(args: Array[String]): Unit = {


    val spark = SparkUtils.getSparkSession("cmcc_pre")
    import spark.implicits._
    val rdd = spark.read.textFile("userprofile/testdata/cmcclog_day01").rdd


    // 加载idmapping字典
    val idm: collection.Map[String, String] = DictLoader.loadIDM(spark, "userprofile/testdata/idm_out_1").collectAsMap()
    val bc = spark.sparkContext.broadcast[collection.Map[String, String]](idm)

    // 加载url内容知识库
    val urlContentRdd: RDD[(String, (String, String, String, String, String))] = DictLoader.loadUrlContent(spark, "crawler/data/content_info")
    val urlDF = urlContentRdd.map(tp => (tp._1, tp._2._1, tp._2._2, tp._2._3, tp._2._4, tp._2._5))
      .toDF("url", "cat1", "cat2", "cat3", "brand", "title")


    val cmccRDD = rdd.map(_.split("\t", -1))
      // 1. 过滤
      .filter(_.size > 48)

      .map(arr => {
        // 2.抽取字段
        val phone = arr(6)
        val imei = arr(7)
        val imsi = arr(8)

        var url = arr(28)
        val indexof = url.indexOf("?")
        val end = if (indexof == -1) url.size else indexof
        url = url.substring(0, end)

        // 3.集成idmapping中的统一图id
        val idmDict = bc.value
        var gid = ""
        val option_imei = idmDict.get(imei.hashCode.toLong+"")
        val option_imsi = idmDict.get(imsi.hashCode.toLong+"")

        if (option_imei.isDefined) {
          gid = option_imei.get
        } else {
          if (option_imsi.isDefined) {
            gid = option_imsi.get
          }
        }

        (gid, phone, imei, imsi, url)
      })


    // 4.集成url内容知识
    val res = cmccRDD.toDF("gid", "phone", "imei", "imsi", "url")
      .join(urlDF, "url")


    res.printSchema()
    res.show(10, false)

    res.write.parquet("userprofile/preout/cmccout")


    spark.close()
  }
}
