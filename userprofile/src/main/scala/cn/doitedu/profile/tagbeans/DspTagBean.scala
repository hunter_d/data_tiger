package cn.doitedu.profile.tagbeans

/**
 * @date: 2019/7/2
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  DSP数据用户画像标签编程模型
 */
case class DspTagBean(
                       gid: String,
                       // 各类id
                       ids: Map[String, Map[String, Double]] = Map.empty[String, Map[String, Double]],
                       // 基本信息标签：省市区商圈，年龄，性别，手机号
                       base_tags: Map[String, Map[String, Double]] = Map.empty[String, Map[String, Double]],
                       // dsp广告推送相关业务标签：广告渠道商id，广告栏位名称
                       dsp_ad_tags: Map[String, Map[String, Double]] = Map.empty[String, Map[String, Double]],
                       // 设备属性标签：手机型号、操作系统、联网类型
                       device_tags: Map[String, Map[String, Double]] = Map.empty[String, Map[String, Double]],
                       // app属性标签：app名称，app描述关键词
                       app_tags: Map[String, Map[String, Double]] = Map.empty[String, Map[String, Double]],
                       // 兴趣标签：受众所访问的页面描述的关键词
                       interest_tags: Map[String, Map[String, Double]] = Map.empty[String, Map[String, Double]]
                     )
