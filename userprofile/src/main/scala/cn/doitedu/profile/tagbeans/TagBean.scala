package cn.doitedu.profile.tagbeans

import java.util

import scala.beans.BeanProperty

case class TagBean(
      @BeanProperty // 用注解标记的属性，会自动生成get、set方法
      var gid: String,
      @BeanProperty
      var tags: util.Map[String, util.Map[String, util.Map[String, Double]]]
)

