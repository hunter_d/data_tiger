package cn.doitedu.restservice.controller;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    // 这种风格的api 叫做restapi
    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable  String name){

        return "hello " + name;
    }



    @RequestMapping("/hello/add/{a}/{b}")
    public String add(@PathVariable int a, @PathVariable int b){

        return (a+b)+"";
    }
}
