package cn.doitedu.restservice.controller;

import cn.doitedu.restservice.service.MathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MathController {

    @Autowired
    MathService mathService;

    @RequestMapping("/math/add")
    public String add(String a,String b){

        int res = mathService.add(a, b);
        return res+"";

    }


    @RequestMapping("/math/multiply")
    public String multiply(String a,String b){

        int res = mathService.multiply(a, b);
        return res+"";

    }


    @RequestMapping("/math/max")
    public String max(String a,String b){

        int res = mathService.max(a, b);
        return res+"";

    }

}
