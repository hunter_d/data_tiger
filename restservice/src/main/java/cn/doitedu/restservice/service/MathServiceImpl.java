package cn.doitedu.restservice.service;


import org.springframework.stereotype.Service;

@Service
public class MathServiceImpl implements MathService {
    @Override
    public int add(String a, String b) {
        return Integer.parseInt(a) + Integer.parseInt(b);
    }

    @Override
    public int multiply(String a, String b) {
        return Integer.parseInt(a) * Integer.parseInt(b);
    }

    @Override
    public int max(String a, String b) {
        int i = Integer.parseInt(a);
        int j = Integer.parseInt(b);

        return i>j?i:j;
    }
}
