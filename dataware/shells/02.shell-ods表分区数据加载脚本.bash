#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

hql="load data inpath '/preout/${dt}/' into table tiger.ods_traffic_log partition(dt='${dt}')"

${HIVE_HOME}/bin/hive -e "${hql}"

if [[ $? = 0 ]];
then echo "ods_traffic_log 分区数据加载成功........."
else
echo "ods_traffic_log 分区数据加载失败.............."
fi