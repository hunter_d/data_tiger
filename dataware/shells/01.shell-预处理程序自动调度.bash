#!/bin/bash

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
jarpath=$1
else
echo '请给定任务jar包的路径'
fi


if [ $2 ];
then
dt=$2
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

export SPARK_HOME=/opt/apps/spark-2.3.3

${SPARK_HOME}/bin/spark-submit  \
--master yarn \
--class cn.doitedu.dw.etl.PreProcess \
${jarpath} \
/logs/${dt} \
/dicts/area.snappy.parquet \
/dicts/biz.snappy.parquet \
/preout/${dt} \
/gps/${dt}


