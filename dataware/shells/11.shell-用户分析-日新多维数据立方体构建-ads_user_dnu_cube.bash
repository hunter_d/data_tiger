#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive
title='DWS日新多维数据立方体cube构建'

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

hql="
insert into table tiger.ads_user_dnu_cube partition (dt='${dt}')
select
count(uid) as u_cnts,
province,
city,
district,
osname,
appver,
week,month,quarter
from tiger.dws_user_dnu where dt='${dt}'
group by province,city,district,osname,appver,week,month,quarter
with cube
" 


${HIVE_HOME}/bin/hive -e "${hql}"


if [[ $? = 0 ]];
then echo "${title} 计算成功........."
else
echo "${title} 计算失败.............."
fi

## 请求邮件服务器发送邮件