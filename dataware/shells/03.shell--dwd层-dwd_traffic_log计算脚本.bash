#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

hql="
insert into table tiger.dwd_traffic_log partition(dt='${dt}')
select
case 
when account is not null and account!='' then account
when imei is not null and imei!='' then imei
when androidId is not null and androidId!='' then androidId
when deviceId is not null and deviceId!='' then deviceId
end  as uid,
account,
imei ,
osName ,
osVer ,
resolution ,
androidId ,
manufacture ,
deviceId ,
appid ,
appVer ,
release_ch ,
promotion_ch ,
areacode ,
longtitude ,
latitude ,
carrier ,
netType ,
cid_sn ,
sessionId ,
logType ,
commit_time ,
from_unixtime(cast(commit_time/1000 as bigint),'yyyy-MM-dd HH:mm:ss') as timestr,
province ,
city ,
district ,
county ,
biz ,
eventMap 
from tiger.ods_traffic_log t
where dt='${dt}'
"

${HIVE_HOME}/bin/hive -e "${hql}"

if [[ $? = 0 ]];
then echo "dwd_traffic_log etl成功........."
else
echo "dwd_traffic_log etl失败.............."
fi

