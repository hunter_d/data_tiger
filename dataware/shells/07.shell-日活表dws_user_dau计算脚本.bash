#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive
title='DWS日活明细表计算'

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

hql="
set hive.exec.dynamic.partition.mode=nonstrict;
insert into table tiger.dws_user_dau partition(dt)
select 
uid,
province,
city,
district,
osname,
appver,
concat(weekofyear(dt),'周') as week,
month(dt) as month,
floor(cast(substr(dt,6,2) as int)/3.1)+1 as quarter,
dt
from tiger.dws_traffic_agg_user
where dt='${dt}'
" 


${HIVE_HOME}/bin/hive -e "${hql}"


if [[ $? = 0 ]];
then echo "${title} 计算成功........."
else
echo "${title} 计算失败.............."
fi

## 请求邮件服务器发送邮件