#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi


hql="
insert into table tiger.dws_traffic_agg_user partition(dt='${dt}')
select
uid,
count(1) as session_cnts,
sum(end_time-start_time) as access_time_amt,
sum(pv_cnts) as pv_cnts,

max(province) as province,
max(city) as city,
max(district) as district,
max(county) as county,
collect_set(biz)[0] as biz,
max(manufacture) as manufacture,
max(osname) as osname,
max(osver) as osver,
max(appver) as appver

from tiger.dwd_traffic_agg_session
where dt='${dt}'
group by uid
" 

${HIVE_HOME}/bin/hive -e "${hql}"


if [[ $? = 0 ]];
then echo "日新计算成功........."
else
echo "日新计算失败.............."
fi

## 请求邮件服务器发送邮件