#!/bin/bash
function bye(){
if [[ $1 != 0 ]]
then 
echo "job $2 failed"
exit 1
fi
}

# 取计算日期
if [ $1 ];
then
export dt=$1
else
export dt=`date -d"-1 day" +'%Y-%m-%d'`
fi


sh 01.shell-预处理程序自动调度.bash /root/job_shells/pre.jar ${dt}
bye $? 01

sh 02.shell-ods表分区数据加载脚本.bash ${dt}
bye $? 02

sh 03.shell--dwd层-dwd_traffic_log计算脚本.bash ${dt}
bye $? 03

sh 04.shell-流量会话明细表脚本-dwd_traffic_agg_session.bash ${dt}
bye $? 04

sh 05.shell-流量用户明细表脚本--dws_traffic_agg_user.bash ${dt}
bye $? 05

sh 06.shell-流量主题报表-流量概况多维数据立方体-ads_traffic_summary_cube.bash ${dt}
bye $? 06

sh 07.shell-日活表dws_user_dau计算脚本.bash ${dt}
bye $? 07

sh 08.shell-用户分析-日活多维数据cube构建-ads_user_dau_cube.bash ${dt}
bye $? 08

sh 09.shell-用户分析-历史记录明细-dws_user_hisu.bash ${dt}
bye $? 09

sh 10.shell-用户分析-日新明细表计算-dws_user_dnu.bash ${dt}
bye $? 10

sh 11.shell-用户分析-日新多维数据立方体构建-ads_user_dnu_cube.bash ${dt}
bye $? 11
