#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive
title='DWS日新明细表计算'

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

hql="
insert into table tiger.dws_user_dnu partition(dt='${dt}')

select 
buid as uid,
province,
city,
district,
osname,
appver,
week,
month,
quarter

from 

(
select
b.uid as buid,
a.uid as auid,
province,
city,
district,
osname,
appver,
b.week,
b.month,
b.quarter
b.dt as dt
from tiger.dws_user_dau b left join tiger.dws_user_hisu a
on b.dt='${dt}' and a.dt=date_sub('${dt}',1) and b.uid = a.uid
) o

where o.auid is null
" 


${HIVE_HOME}/bin/hive -e "${hql}"


if [[ $? = 0 ]];
then echo "${title} 计算成功........."
else
echo "${title} 计算失败.............."
fi

## 请求邮件服务器发送邮件