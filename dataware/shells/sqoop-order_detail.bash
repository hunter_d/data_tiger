#!/bin/bash

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

echo ">>>>>>开始从mysql导数据......>>>>>>"

# 导入数据到hdfs
/opt/apps/sqoop/bin/sqoop import \
--connect jdbc:mysql://c701:3306/doit_mall \
--username root \
--password Root.123 \
--target-dir /doit_mall/order_detail/${dt} \
--fields-terminated-by '\001' \
--table order_detail \
--split-by id \
--incremental append \
--check-column id \
--last-value 55928 \
--m 2

# 将数据load到hive的ods_user_info表中
echo ">>>>>>开始将数据load进hive表......>>>>>>"
/opt/apps/hive/bin/hive -e "load data inpath '/doit_mall/order_detail/${dt}' into table tiger.ods_order_detail partition(dt='${dt}')"
