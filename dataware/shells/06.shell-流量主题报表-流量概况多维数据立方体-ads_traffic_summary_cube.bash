#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive
title='流量概况多维数据立方体计算'

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

echo ">>>>>>>>> 准备开始 ${dt} 的  ${title} >>>>>>>>>>>>>>>>>>>>>>"


hql="
set hive.exec.dynamic.partition.mode=nonstrict;

insert into table tiger.ads_traffic_summary_cube partition(dt='${dt}')
select
province,city,district,county,osname,osver,appver,
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio,

-- lpad(bin(cast(GROUPING__ID as bigint)),7,'0') as dim

from  tiger.dws_traffic_agg_user t where dt = '${dt}'
-- 计算7个维度的所有组合
group by province,city,district,county,osname,osver,appver
-- 指定所需要的维度组合
-- grouping sets((province,osname),(province,appver),(osname,appver),(),(province))
with cube
" 

${HIVE_HOME}/bin/hive -e "${hql}"


if [[ $? = 0 ]];
then echo "${title} 计算成功........."
else
echo "${title} 计算失败.............."
fi

## 请求邮件服务器发送邮件