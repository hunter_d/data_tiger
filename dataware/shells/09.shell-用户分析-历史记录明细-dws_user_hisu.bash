#!/bin/bash
## 先获取当前日期的前一天日期

## 设置环境变量
export HIVE_HOME=/opt/apps/hive
title='DWS历史用户记录明细'

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

hql="
insert into table tiger.dws_user_hisu partition(dt='${dt}')
select
if(a.uid is null,b.uid,a.uid)  as uid,  --取uid
if(a.uid is not null,a.ts_first,b.dt) as ts_first, -- 取首次登陆时间：只要a表有，则首次登陆时间取自a表；否则取自b表
if(b.uid is not null,b.dt,a.ts_last) as ts_last -- 取最后登陆时间：只要b表有，则取自b表；否则取a表

from 
(select *
from 
tiger.dws_user_hisu 
where dt=date_sub('${dt}',1)) a  --前一天的历史记录

full join

(select *
from 
tiger.dws_user_dau 
where dt='${dt}') b
on a.uid=b.uid
" 


${HIVE_HOME}/bin/hive -e "${hql}"


if [[ $? = 0 ]];
then echo "${title} 计算成功........."
else
echo "${title} 计算失败.............."
fi

## 请求邮件服务器发送邮件