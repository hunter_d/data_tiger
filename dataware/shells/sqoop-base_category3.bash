#!/bin/bash

## 判断是否通过参数传入了计算日期
if [ $1 ];
then
dt=$1
else
dt=`date -d"-1 day" +'%Y-%m-%d'`
fi

echo ">>>>>>开始从mysql导数据......>>>>>>"

# 导入数据到hdfs
/opt/apps/sqoop/bin/sqoop import \
--connect jdbc:mysql://c701:3306/doit_mall \
--username root \
--password Root.123 \
--target-dir /doit_mall/base_category3/${dt} \
--fields-terminated-by '\001' \
--table base_category3 \
--split-by id \
--m 2

# 将数据load到hive的ods_user_info表中
echo ">>>>>>开始将数据load进hive表......>>>>>>"
/opt/apps/hive/bin/hive -e "load data inpath '/doit_mall/base_category3/${dt}' into table tiger.ods_base_category3 partition(dt='${dt}')"
