import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
case class P(val id:Int,val name:String,val s:String)


object DsDemo {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().appName("x").master("local").getOrCreate()
    import spark.implicits._
    import org.apache.spark.sql.functions._

    val p1 = new P(1,"a","16.6")
    val p2 = new P(2,"b","26.6")
    val ds: Dataset[P] = spark.createDataset(Seq(p1,p2))

    ds.select("id")
    ds.select(upper('id))

    ds.selectExpr("upper(id)","name").show()

/*

    val df: DataFrame = ds.select("id","name","s")

    ds.map(p=>{
      val id:Int = p.id
      (id)
    })
      .toDF("id").show(10,false)



    df.map(row=>{
      val id: Float = row.getAs[Float]("name")
      (id)
    })
      .toDF("id").show(10,false)



    val rdd: RDD[P] = ds.rdd
    rdd.toDF();

    val t:Tuple2[Int,Int] = (1,3)


    val x:Any = 1;
    println(x)
*/


    spark.close()

  }



}
