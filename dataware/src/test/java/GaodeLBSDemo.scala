import java.util

import com.alibaba.fastjson.JSON
import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClientBuilder

/**
  * web服务接口的请求，关键点有3个：
  * 1.用什么工具发送http请求-- java中比较通用的工具有apache的httpclient
  * 2.接口文档中的参数说明
  * 3.接口文档中的响应结果格式说明
  */
object GaodeLBSDemo {

  def main(args: Array[String]): Unit = {

    import scala.collection.JavaConversions._

    // 构造一个http客户端
    val client = HttpClientBuilder.create().build()


    // 构造也给get协议请求
    val get = new HttpGet("https://restapi.amap.com/v3/geocode/regeo?key=66f3031018c358750fa6295eb576e718&location=116.481488,39.990464")
    //val get = new HttpGet("https://www.baidu.com")

    // 用客户端去执行请求
    val response = client.execute(get)

    val entity = response.getEntity
    val in = entity.getContent

    val lines: util.List[_] = IOUtils.readLines(in)

    // 后续 TODO 解析响应回来的json，取出省、市、区、街道、商圈


  }

}
