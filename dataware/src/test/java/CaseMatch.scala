
/**
 * @date: 2019/6/14
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  讲解模式匹配的经典案例
 */

/**
  * 这里相当于自己造了一个样例类
  *
  * @param name
  * @param age
  */
class Person(val name: String, val age: Int) extends Serializable {

}

object Person {

  def apply(name: String, age: Int): Person = new Person(name, age)

  def unapply(p: Person): Option[(String, Int)] = {

    Some(p.name, p.age)
  }
}


// 样例类，就是内部自己封装了 apply方法，unapply方法，实现了序列化接口
case class Person2(name: String, age: Int)


object CaseMatch {

  def main(args: Array[String]): Unit = {

    val s = "hello"
    s match {
      case "hello" => println("他是一个hello字符串")
      case x: String => println("他是一个字符串: " + x)
    }


    val p = Person("桂如涛", 16)
    p match {
      // 匹配类型
      case p: Person => {
        println("这是一个Person类的对象")

      }
      // 利用模式匹配抽取数据
      case Person(x: String, y: Int) => println(x + " " + y)

      case _ =>
    }


    val f: Person=>String  = { case Person(x:String,y:Int)=> x }


  }

}
