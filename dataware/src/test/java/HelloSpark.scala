import org.apache.spark.sql.SparkSession

object HelloSpark {


  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder().appName("hello").master("local[*]").getOrCreate()
    import spark.implicits._

    val ds = spark.createDataset(Seq("a","b","c","b","c"))
    ds.toDF("word")
      .groupBy("word")
      .count()
      .show(10,false)



    spark.close()
  }


}
