import ch.hsr.geohash.GeoHash;

public class GeohashDemo {


    public static void main(String[] args) {


        String geohash1 = GeoHash.withCharacterPrecision(45.667, 160.876547, 8).toBase32();
        String geohash2 = GeoHash.withCharacterPrecision(45.667, 160.896558, 8).toBase32();
        System.out.println(geohash1);
        System.out.println(geohash2);


    }
}
