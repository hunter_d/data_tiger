package cn.doitedu.dw.etl

import java.util

import ch.hsr.geohash.GeoHash
import cn.doitedu.common.beans.{LogBean, LogBeanV2}
import cn.doitedu.common.utils.SparkUtils
import com.alibaba.fastjson.JSON
import org.apache.commons.lang3.StringUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.types.{DataType, DataTypes, StructField, StructType}

import scala.collection.{immutable, mutable}

/**
  * @date: 2019/6/14
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 公司内部数据：流量日志  --》 预处理程序
  */
object PreProcess {

  def main(args: Array[String]): Unit = {
    Logger.getLogger("org").setLevel(Level.WARN)

    val spark = SparkUtils.getSparkSession("preprocess")
    import spark.implicits._
    import scala.collection.JavaConversions._


    // 此时此刻，应该是06-13日 00:30分，读前一天的（2019-06-12）流量日志
    val dsLog: Dataset[String] = spark.read.textFile(args(0))
    dsLog.show(10, false)


    // 加载行政区域地理位置字典，并收集为单机集合
    val dsArea: DataFrame = spark.read.parquet(args(1))
    // 将字典rdd收集为本地的单机集合hashmap
    val areaMap: Map[String, (String, String, String, String)] = dsArea.collectAsList().map({ case Row(geo: String, p: String, c: String, d: String, t: String) => (geo, (p, c, d, t)) }).toMap
    // 然后广播出去
    val areaBC = spark.sparkContext.broadcast[Map[String, (String, String, String, String)]](areaMap)


    // 加载商圈地理位置字典，并收集为单机集合，然后广播出去
    val dsBiz = spark.read.parquet(args(2))
    // dsBiz中可能有多个商圈信息对应同一个geo码
    val bizMap: Map[String, List[(String, String, String, String)]] = dsBiz.collectAsList()
      .map({ case Row(geo: String, p: String, c: String, d: String, biz: String) => (geo, (p, c, d, biz)) })
      .groupBy(_._1)
      .mapValues(iter => iter.map(_._2).toList)
      .map(x => x) // 为了防止出现序列化异常

    // 然后广播出去
    val bizBC = spark.sparkContext.broadcast[Map[String, List[(String, String, String, String)]]](bizMap)

    // 逐调用json解析工具方法，逐行处理流量日志：解析json为LogBean
    val beanRdd: RDD[LogBean] = JsonParse.parseJson(dsLog)

    /*
     按照Dataframe的表操作方式来做json解析
    val df = JsonParse.parseJson(dsLog,spark)
    df.printSchema()
    df.show(10,false)
    df.select("u.cookieid","u.account","u.email").show(10,false)*/

    /*查看阶段性成果
    beanRdd.take(10).foreach(println)
    sys.exit()*/


    // 数据清洗过滤
    val washed = beanRdd.filter(_ != null)
      .filter(bean => {
        // 判断数据是否符合规则
        // account && sessionid &&  imei && deviceId && androidId 不能全为空
        StringUtils.isNotBlank(bean.account) ||
          StringUtils.isNotBlank(bean.sessionId) ||
          StringUtils.isNotBlank(bean.imei) ||
          StringUtils.isNotBlank(bean.deviceId) ||
          StringUtils.isNotBlank(bean.androidId)
      }
      )

    /*查看阶段性成果
    washed.take(10).foreach(println)
    sys.exit()*/


    /**
      * 数据集成
      */
    val res = washed.map(bean => {

      // 获取广播变量地域字典
      val areamap = areaBC.value
      // 获取广播变量商圈字典
      val bizmap = bizBC.value

      // 将流量日志中的gps坐标，转换成geohash编码
      val lng = bean.longtitude
      val lat = bean.latitude
      val geo = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 6)

      // 用日志中得到的geohash编码去字典中匹配地域名称、商圈信息
      val biz: Option[List[(String, String, String, String)]] = bizmap.get(geo)
      val area: Option[(String, String, String, String)] = areamap.get(geo)

      // 整理省市区商圈值
      var province = ""
      var city = ""
      var district = ""
      var county = ""
      var bizLst = List[String]()

      // 如果area中有位置信息，则从area中取
      if (area.isDefined) {
        val tp = area.get
        province = tp._1
        city = tp._2
        district = tp._3
        county = tp._4
      } else {
        // 否则从biz字典中取省市区
        if (biz.isDefined) {
          val bizs: immutable.Seq[(String, String, String, String)] = biz.get
          val pcdb = bizs(0)
          province = pcdb._1
          city = pcdb._2
          district = pcdb._3
        }
      }

      // 取商圈信息
      bizLst = biz match {
        case Some(lst) => lst.map(tp => tp._4)
        case None => List[String]()
      }

      // 拼装结果返回
      LogBeanV2(
        bean.cookieid,
        bean.account,
        bean.imei,
        bean.osName,
        bean.osVer,
        bean.resolution,
        bean.androidId,
        bean.manufacture,
        bean.deviceId,
        bean.appid,
        bean.appVer,
        bean.release_ch,
        bean.promotion_ch,
        bean.areacode,
        bean.longtitude,
        bean.latitude,
        bean.carrier,
        bean.netType,
        bean.cid_sn,
        bean.ip,
        bean.sessionId,
        bean.logType,
        bean.commit_time,
        province,
        city,
        district,
        county,
        bizLst,
        bean.eventMap
      )

    }).toDF()

    val toParseGPS = res.filter("province = ''").select("longtitude", "latitude")


    /* 阶段性成果秀
    res.where("size(biz) > 0").show(10,false)
    sys.exit()*/

    // 将结果输出为parquet格式文件（日后可以导入hive的ods层表）
    res.coalesce(1).write.parquet(args(3))

    // 对上面的结果集合，过滤出匹配地理位置不成功的数据中的gps坐标，并保存为文件（以便于日后用高德地图逆地理位置解析服务来获取地理位置信息，丰富知识库）
    toParseGPS.coalesce(1).write.parquet(args(4))

    spark.close()

  }


}
