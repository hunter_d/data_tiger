package cn.doitedu.dw.rpts

import java.text.SimpleDateFormat
import java.time.LocalDate

import cn.doitedu.common.utils.SparkUtils
import org.apache.spark.sql.Row

/**
  * @date: 2019/6/21
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 数仓报表计算程序
  *               用户app使用间隔分布统计  ads_user_interval
  */
object ADS_user_interval {

  def main(args: Array[String]): Unit = {
    val spark = SparkUtils.getSparkSession("ads_user_interval")
    import spark.implicits._

    // 计算日，应该从参数传入，如果没有，则取今日
    val calcDay = "2019-06-19"

    // 读数据
    val activeZip = spark.read.parquet("hdfs://c701:8020/user/hive/warehouse/tiger.db/dws_user_active_zip/dt=2019-06-19")


    // 将用户的多个区间变成一行
    val rdd = activeZip.rdd.map({
      case Row(uid: String, daynew: String, begin_dt: String, end_dt: String) => {
        // 判断区间结束日期是否为9999-12-31，如果是，则替换成计算日
        var end_day = end_dt
        if (end_dt.equals("9999-12-31")) end_day = calcDay

        val bg = begin_dt.split("-")
        val end = end_day.split("-")

        val d2 = LocalDate.of(end(0).toInt, end(1).toInt, end(2).toInt).toEpochDay
        val d1 = LocalDate.of(bg(0).toInt, bg(1).toInt, bg(2).toInt).toEpochDay

        (uid, (d1, d2))
      }

    })
      .groupByKey()
      .mapValues(iter => iter.toList.sortBy(_._1))
      .flatMap(kv => {
        // 用来装： ((uid,间隔类型)，次数)
        val kvLst = new scala.collection.mutable.ListBuffer[((String, String), Long)]
        // 取uid
        val uid = kv._1
        // 取区间列表
        val ranges: List[(Long, Long)] = kv._2

        // 求间隔1天的  kv
        val day1kvs: List[((String, String), Long)] = ranges.map(tp => ((uid, "间隔1天"), tp._2 - tp._1))
        kvLst.++=(day1kvs)

        // 求间隔N天的  kv
        /**
          * (6,7)    9-13   15-18
          * (-1,-1)   6-7    9-13
          */
        val dNkvs: List[((String, String), Long)] = ranges.zip((-1.toLong, -1.toLong) :: ranges).filter(_._2._1 != -1.toLong).map(tp => ((uid, "间隔" + (tp._1._1 - tp._2._2) + "天"), 1.toLong))
        kvLst.++=(dNkvs)

        kvLst
      })


    // 分组聚合
    val df = rdd.reduceByKey(_ + _)
      .map(tp => {
        (tp._1._1, tp._1._2, tp._2.toInt)
      })
      .toDF("uid","interval_type","cnts")


    // 保存结果
    df.write.parquet("hdfs://c701:8020/intvl/")


    spark.close()

  }


}
