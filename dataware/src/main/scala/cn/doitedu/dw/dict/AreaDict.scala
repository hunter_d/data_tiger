package cn.doitedu.dw.dict

import java.util.Properties

import ch.hsr.geohash.GeoHash
import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Row, SparkSession}

object AreaDict {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)


    val spark: SparkSession = SparkUtils.getSparkSession("areadict")
    import spark.implicits._


    val props = new Properties()
    props.setProperty("user", "root")
    props.setProperty("password", "root")
    props.setProperty("driver", "com.mysql.jdbc.Driver")

    // 什么叫做dataframe？  什么叫做dataset ？   什么叫做rdd？
    // schema + rdd[row] =>  dataset[row]  =>  dataframe
    val df = spark.read.jdbc("jdbc:mysql://localhost:3306/dicts?charactorEncoding=utf-8", "area", props)
    //df.printSchema() //  打印dataframe的表结构信息：schema
    //df.show(10, false)

    // 将每一行中的经纬度，转成geohash编码
    val res = df
      // dataframe中引入的新的api： DSL风格api
      .where("lng is not null and lat is not null")
      .map({
        // 模式匹配提取数据
        case Row(lng: Double, lat: Double, province: String, city: String, district: String, county: String) =>
          val geo = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 6)
          (geo, province, city, district, county)
      }).toDF("geo", "province", "city", "district", "county")


    /**
      * 模式匹配函数的第二种写法
      */
    /*df.where("lng is not null and lat is not null")
      .map(row => {
        row match {
          case Row(lng: Double, lat: Double, province: String, city: String, district: String, county: String) =>
        }
      })*/

    /**
      * 模式匹配函数的第三种写法
      */
    /*val f: Row => (String, String, String, String, String) =
        {
          case Row(lng: Double, lat: Double, province: String, city: String, district: String, county: String) =>
            val geo = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 6)
            (geo, province, city, district, county)
        }
    df.where("lng is not null and lat is not null")
      .map(f)*/

    res.coalesce(1).write.parquet("dataware/data/area/output")

    spark.close()


  }
}
