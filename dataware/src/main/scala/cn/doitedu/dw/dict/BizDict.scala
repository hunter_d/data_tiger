package cn.doitedu.dw.dict

import java.time.LocalDate

import ch.hsr.geohash.GeoHash
import cn.doitedu.common.utils.SparkUtils
import org.apache.spark.sql.Dataset

object BizDict {

  def main(args: Array[String]): Unit = {
    val spark = SparkUtils.getSparkSession("bizdict")
    import spark.implicits._

    val ds: Dataset[String] = spark.read.textFile("dataware/data/area/input/商圈数据.sql")
    val res = ds.map(line => {
      val arr: Array[String] = line.split("'")
      val province = arr(3)
      val city = arr(5)
      val district = arr(7)
      val biz = arr(9)
      val lng = arr(11).toDouble
      val lat = arr(13).toDouble

      val geo = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 6)

      (geo, province, city, district, biz)
    })
      .toDF("geo", "province", "city", "district", "biz")

    res.show(10,false)

    res.write
      .parquet("dataware/data/area/bizout")






    spark.close()


  }

}
