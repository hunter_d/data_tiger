/* ods_user_info   建表语句 */
CREATE TABLE `ods_user_info` (
  `id` bigint COMMENT '编号',
  `login_name` string COMMENT '用户名称',
  `nick_name` string COMMENT '用户昵称',
  `passwd` string COMMENT '用户密码',
  `name` string COMMENT '用户姓名',
  `phone_num` string COMMENT '手机号',
  `email` string COMMENT '邮箱',
  `head_img` string COMMENT '头像',
  `user_level` string COMMENT '用户级别',
  `birthday` date COMMENT '用户生日',
  `gender` string COMMENT '性别 M男,F女',
  `create_time` string COMMENT '创建时间'
)
partitioned by (dt string)
row format delimited fields terminated by '\001'
;

/* ods_base_category1   建表语句 */
CREATE TABLE `ods_base_category1` (
  `id` bigint COMMENT '编号',
  `name` string COMMENT '分类名称'
)
partitioned by (dt string)
row format delimited fields terminated by '\001'
;

/* ods_base_category2   建表语句 */
CREATE TABLE `ods_base_category2` (
  `id` bigint  COMMENT '编号',
  `name` string COMMENT '二级分类名称',
  `category1_id` bigint COMMENT '一级分类编号'
)
partitioned by (dt string)
row format delimited fields terminated by '\001'
;

/* ods_base_category3   建表语句 */
CREATE TABLE `ods_base_category3` (
  `id` bigint  COMMENT '编号',
  `name` string COMMENT '3级分类名称',
  `category2_id` bigint COMMENT '2级分类编号'
)
partitioned by (dt string)
row format delimited fields terminated by '\001'
;


/* ods_order_detail 建表语句*/
CREATE TABLE `ods_order_detail` (
  `id` bigint  COMMENT '编号',
  `order_id` bigint COMMENT '订单编号',
  `sku_id` bigint COMMENT 'sku_id',
  `sku_name` string COMMENT 'sku名称（冗余)',
  `img_url` string COMMENT '图片名称（冗余)',
  `order_price` double COMMENT '购买价格(下单时sku价格）',
  `sku_num` string COMMENT '购买个数'
) 
partitioned by (dt string)
row format delimited fields terminated by '\001'
;


/* */
CREATE TABLE `ods_order_info` (
  `id` bigint  COMMENT '编号',
  `consignee` string COMMENT '收货人',
  `consignee_tel` string COMMENT '收件人电话',
  `total_amount` double COMMENT '总金额',
  `order_status` int COMMENT '订单状态',
  `user_id` bigint  COMMENT '用户id',
  `payment_way` string  COMMENT '付款方式',
  `delivery_address` string COMMENT '送货地址',
  `order_comment` string COMMENT '订单备注',
  `out_trade_no` string COMMENT '订单交易编号（第三方支付用)',
  `trade_body` string COMMENT '订单描述(第三方支付用)',
  `create_time` string COMMENT '创建时间',
  `operate_time` string COMMENT '操作时间',
  `expire_time` string COMMENT '失效时间',
  `tracking_no` string COMMENT '物流单编号',
  `parent_order_id` bigint  COMMENT '父订单编号',
  `img_url` string COMMENT '图片路径'
) 
partitioned by (dt string)
row format delimited fields terminated by '\001'
;

/* */
CREATE TABLE `ods_sku_info` (
  `id` bigint  COMMENT '库存id(itemID)',
  `spu_id` bigint  COMMENT '商品id',
  `price` double COMMENT '价格',
  `sku_name` string COMMENT 'sku名称',
  `sku_desc` string  COMMENT '商品规格描述',
  `weight` double COMMENT '重量',
  `tm_id` bigint  COMMENT '品牌(冗余)',
  `category3_id` bigint COMMENT '三级分类id（冗余)',
  `sku_default_img` string COMMENT '默认显示图片(冗余)',
  `create_time` string COMMENT '创建时间'
) 
partitioned by (dt string)
row format delimited fields terminated by '\001'
;



