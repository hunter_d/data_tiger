
/*
   广告效果分析：ADS层，广告概况报表建模
   建模：竖表
*/
create  table ads_ad_overall(
dt string,
adid string,
event_type string,
cnts int comment '次数',
users int comment '人数',
max_url string comment '人数最大页'
)
stored as parquet;



/*
   ETL开发
   1.源表：广告相关事件明细表 dws_ad_event_detail
   2.逻辑： 
*/
with tmp as (
select
uid,
event_type,
eventmap['url'] as url,
eventmap['adid'] as adid

from dws_ad_event_detail where dt='2019-06-15'
)


/*
  步骤1： 按最细粒度组合聚合
    一个广告在一个页面上的一个行为类型的发生次数和人数
*/
select
adid,
url,
event_type,
count(1) as cnts
count(distinct uid) as users


from tmp
group by adid,url,event_type

/*
ad01,/ab/c/,ad_click,80,10
ad01,/ab/c/,ad_show,1000,500
ad01,/ab/d/,ad_click,180,160
ad01,/ab/e/,ad_show,80,60
*/



/*
  步骤2： 对上面的结果，按人数大小标记序号，并序号标记在url上
*/

select 
adid,
url,
event_type,
cnts,
users,
concat(row_number() over(partition by adid,event_type order by users),'-',url)
from
(
select
adid,
url,
event_type,
count(1) as cnts,
count(distinct uid) as users
from tmp
group by adid,url,event_type
) o


/*
ad01,1-/ab/c/,ad_click,80,10,  
ad01,2-/ab/d/,ad_click,180,160 

ad01,1-/ab/c/,ad_show,1000,500
ad01,2-/ab/e/,ad_show,80,60   
*/


/*
  步骤3： 对上面的结果，按adid和eventtype聚合
  求出：一个广告，一种行为类型下的，次数和人数以及最大页
*/

-- 

insert into table ads_ad_overall
select
'2019-06-15',
adid,
event_type,
sum(cnts) cnts,
sum(users) users,
split(max(url),'-')[1] as max_url

from 
(
select 
adid,
event_type,
cnts,
users,
concat(row_number() over(partition by adid,event_type order by users),'-',url) as url
from
(
select
adid,
url,
event_type,
count(1) as cnts,
count(distinct uid) as users
from tmp
group by adid,url,event_type
) o1
) o2

group by adid,event_type
;
/*
ad01,ad_click,260,170,/ab/d/
ad01,ad_show,1080,560,/ab/e
*/



/*
  步骤4： 对上面的结果，将行为类型竖表变横表
  将同一个广告的两种行为的统计结果放入一行
  这个查询结果，可以存在一个表中，页面就只要直接查询该表即可
  也可以这个查询工作直接交给前端去做
*/
with tmp1 as (
select *  from ads_ad_overall where dt ='2019-06-15' and event_type='ad_show'
),
tmp2 as(
select *  from ads_ad_overall where dt ='2019-06-15' and event_type='ad_click'
)


select
a.adid,
a.cnts as show_cnts,
a.users as show_users,
a.max_url as show_max_url,
b.cnts as click_cnts,
b.users as click_users,
b.max_url as click_max_url
from  tmp1 a
join  tmp2 b
on a.adid=b.adid
 











