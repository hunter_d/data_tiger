/*  
   新增用户数，统计报表
   多维cube
   建模：ads_user_dnu_cube
*/
drop table if exists ads_user_dnu_cube;
create table ads_user_dnu_cube(
u_cnts  int,
province string,
city string,
district string,
osname string,
appver string,
week string,
month string,
quarter string
)
partitioned by (dt string)
stored as parquet;


/*
	ETL开发
	源表： 日新明细表  dws_user_dnu
	计算逻辑： 按各种维度组合聚合新用户数量

*/
insert into table ads_user_dnu_cube partition (dt='2019-06-13')
select
count(uid) as u_cnts,
province,
city,
district,
osname,
appver,
week,month,quarter
from dws_user_dnu where dt='2019-06-13'
group by province,city,district,osname,appver,week,month,quarter
with cube
;

/*
  ads层cube结果数据，计算完成后还是存储在hive中（hdfs）
  前端数据可视化平台查询不便！
  需要将ads的报表结果数据导入各类数据库系统（比如mysql、hbase+phoenix）
*/


