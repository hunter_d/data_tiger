/*
    用户新鲜度报表
	建模：以竖表的形式来实现
	ads_user_fresh
*/
drop table if exists ads_user_fresh;
create table ads_user_fresh(
dt string,
act_amt int comment '当日活跃总数',
new_days int comment '新鲜天数（新增日距今天数）',
ratio double comment '构成占比'
)
stored as parquet;

/*
   ETL开发
   计算逻辑：
	源表：历史用户记录明细表  dws_user_hisu
	逻辑：统计各种新增日期下的新增数/当日的总活跃数
*/

-- 注册一张临时表，以便于后续可以反复使用
with 
 tmp as (select * from  dws_user_hisu  where ts_last='2019-06-15')
 
insert into table ads_user_fresh
select
'2019-06-15' as dt,
count(1) as act_amt,
'new_1d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',1),1,0))/count(1) as ratio
from tmp

union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_2d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',2),1,0))/count(1) as ratio
from tmp


union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_3d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',3),1,0))/count(1) as ratio
from tmp

union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_4d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',4),1,0))/count(1) as ratio
from tmp


union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_5d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',5),1,0))/count(1) as ratio
from tmp


union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_6d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',6),1,0))/count(1) as ratio
from tmp

union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_7d' as new_days,
sum(if(ts_first=date_sub('2019-06-15',7),1,0))/count(1) as ratio
from tmp

union all

select
'2019-06-15' as dt,
count(1) as act_amt,
'new_7dup' as new_days,
sum(if(ts_first<date_sub('2019-06-15',7),1,0))/count(1) as ratio
from tmp






