/*
   广告效果分析：DWS层广告相关事件明细表
   建模：dws_ad_event_detail
   本表的功能：从日志中提取出，所有跟广告行为有关的事件
     什么叫做跟广告行为有关？
		广告曝光、
		广告点击及携带了广告跟踪码的其他事件
		（广告跟踪码是用户点击了一个广告后产生的）
*/
drop table if exists dws_ad_event_detail;
create table dws_ad_event_detail(
uid string,
sessionid string,
event_type string,
commit_time bigint,
eventmap map<string,string>
)
partitioned by (dt string)
stored as parquet
;

/*
	ETL开发
	1.源表：流量日志明细表
	2.计算逻辑：
		过滤：过滤出ad_show事件，以及带ad_trace_id的事件
*/

insert into table dws_ad_event_detail partition(dt='2019-06-15')
select
uid,
sessionid,
logtype as event_type,
commit_time,
eventmap
from dwd_traffic_log
where dt='2019-06-15'
and logtype='ad_show'
or
(eventmap['ad_trace_id'] is not null and trim((eventmap['ad_trace_id'])!='')