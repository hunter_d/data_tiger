/*
   访问路径分析DWD层
	建模：dwd_routes_detail
   
*/
drop table if exists dwd_routes_detail;
create table dwd_routes_detail(
uid string,
sessionid string,
url string,
step int,
pre_url string
)
partitioned by (dt string)
stored as parquet
;


/*
   ETL开发：
	1.源表：事件明细表dwd_event_detail
	2.计算逻辑：按照时间先后顺序，为每次页面浏览标记序号
				将上一条的页面拉到下一条，作为pre_url
*/
insert into table tiger.dwd_routes_detail partition(dt='2019-06-15')

select
uid,
sessionid,
location as url,
row_number() over(partition by uid,sessionid order by commit_time) as step,
lag(location,1,null) over(partition by uid,sessionid order by commit_time) as pre_url

from tiger.dwd_event_detail
where dt='2019-06-15' and event_type='pg_view'
;