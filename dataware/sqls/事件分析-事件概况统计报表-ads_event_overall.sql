/*
   ADS层，事件概况统计报表 ads_event_overall
*/
create table ads_event_overall(
event_type string,
event_name string,
cnts_yesterday int,
cnts_today int,
users_yesterday int,
users_today int

)
stored as parquet;


/*
   ETL开发
   计算逻辑：
	1.源表 事件明细用户聚合表：dws_event_overall_agg_u
	2.逻辑： 汇总求和
*/
insert into table ads_event_overall
select
event_type,
case event_type
 when 'act_click'    then '活动点击'
 when 'act_join'     then '活动参与'
 when 'act_register' then '活动注册'
 when 'ad_click'     then '广告点击'
 when 'ad_show'      then '广告曝光'
 when 'addcart'      then '添加购物车'
 when 'favor'        then '收藏'
 when 'order_pay'    then '订单支付'
 when 'order_sub'    then '订单提交'
 when 'pg_view'      then '页面浏览'
 when 'rate'         then '评分'
 when 'search'       then '搜索'
 when 'startup'      then '启动'
 when 'thumbup'      then '点赞'
 else '其他'
end as event_name,
sum(if(dt='2019-06-15',cnts,0)) as cnts_yesterday,
sum(if(dt='2019-06-16',cnts,0)) as cnts_today,
sum(if(dt='2019-06-15',1,0)) as users_yesterday,
sum(if(dt='2019-06-16',1,0)) as users_today


from dws_event_overall_agg_u
where dt='2019-06-16' or  dt='2019-06-15'

group by event_type
;


