/*
  日活表：建模
  
*/
drop table if exists dws_user_dau;
create table dws_user_dau(
uid string,
province string,
city string,
district string,
osname string,
appver string,
week string,
month string,
quarter string
)
partitioned by (dt string)
stored as parquet
;


/*
   ETL开发：
	来源表：dws_traffic_agg_user
	计算逻辑： 抽取
*/
insert into table dws_user_dau partition(dt)
select 
uid,
province,
city,
district,
osname,
appver,
concat(weekofyear(dt),'周') as week,
month(dt) as month,
floor(cast(substr(dt,6,2) as int)/3.1)+1 as quarter,
dt
from dws_traffic_agg_user
where dt='2019-06-13'
;