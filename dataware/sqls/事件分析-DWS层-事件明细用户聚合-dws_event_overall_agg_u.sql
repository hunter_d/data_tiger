/*
   DWS层，事件概况统计(按用户聚合) dws_event_overall_agg_u
*/

create table dws_event_overall_agg_u(
event_type string,
uid string,
cnts int
)
partitioned by (dt string)
stored as parquet;

/*
   ETL开发
   计算逻辑：
   1. 源表，事件次数会话聚合表：dws_event_overall_agg_session
   2. 逻辑：按用户聚合
*/

insert into table dws_event_overall_agg_u partition(dt='2019-06-16')
select
event_type,
uid,
sum(cnts) as cnts
from 
dws_event_overall_agg_session
where dt='2019-06-16'
group by event_type,uid
;