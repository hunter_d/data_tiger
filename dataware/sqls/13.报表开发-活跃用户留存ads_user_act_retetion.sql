/*
    活跃用户留存报表
	建模：竖表
	ads_user_act_retention
*/
create table ads_user_act_retention(
dt string,
act_cnts int,
retention_days string,
retention_cnts int
)
stored as parquet;


/*
    方案1：ETL 开发
	计算逻辑：
		1. 源表： 各天的日活表
		2. 逻辑： 滚动计算，计算前N天到当天的，活跃留存数
		          比如，6.6号，算6.5-6.6的1日留存
				        6.7号，算6.5-6.7的2日留存，算6.6-6.7的1日留存
						6.8号，算6.5-6.8的3日留存，算6.6-6.8的2日留存，算6.7-6.8的1日留存
						......
						
			将	6.5的日活 join 6.8的日活，求count  --》 得到6.5号的3日后活跃留存
                6.6的日活 join 6.8的日活，求count  --》 得到6.6号的2日后活跃留存
                6.7的日活 join 6.8的日活，求count  --》 得到6.7号的1日后活跃留存				
*/




/*
    方案2：ETL 开发
	计算逻辑：
		1. 源表： 用户活跃记录拉链表
		2. 逻辑： 滚动计算，计算前N天到当天的，活跃留存数
		          比如，6.6号，算6.5-6.6的1日留存
				        6.7号，算6.5-6.7的2日留存，算6.6-6.7的1日留存
						6.8号，算6.5-6.8的3日留存，算6.6-6.8的2日留存，算6.7-6.8的1日留存
						......
*/


/*

a,15,16
a,18,99
b,16,99
c,17,18
d,14,15
d,17,18
d,20,99
e,20,99
f,17,19
*/

同一个人，有一个区间包含18号，且，有一个区间包含20号

/*
   算法验证：
   用测试数据来检验计算的逻辑：18号的活跃用户，在20号还有几个
*/
-- 测试数据表
+---------+-------------+-----------+
| t1.uid  | t1.begindt  | t1.enddt  |
+---------+-------------+-----------+
| a       | 15          | 16        |
| a       | 18          | 99        |
| b       | 16          | 99        |
| c       | 17          | 18        |
| d       | 14          | 15        |
| d       | 17          | 18        |
| d       | 20          | 99        |
| e       | 20          | 99        |
| f       | 17          | 19        |
+---------+-------------+-----------+


-- 步骤一，将每一个人的最后一个区间，添加到其他各个区间
select

uid,
begindt,
enddt,
last_value(begindt) over(partition by uid order by enddt rows between unbounded preceding and unbounded following) as maxbegin,
last_value(enddt) over(partition by uid order by enddt rows between unbounded preceding and unbounded following) as maxenddt

from t1
;

+---------+-------------+-----------+
| t1.uid  | t1.begindt  | t1.enddt  |
+---------+-------------+-----------+
| a       | 15          | 16        |    18   99
| a       | 18          | 99        |    18   99
| b       | 16          | 99        |    16   99
| c       | 17          | 18        |    17   18
| d       | 14          | 15        |    20   99
| d       | 17          | 18        |    20   99
| d       | 20          | 99        |    20   99
| e       | 20          | 99        |
| f       | 17          | 19        |
+---------+-------------+-----------+


-- 步骤二：判断区间是否包含所指定的天数，来求目标人数
select
count (distinct uid)  as cnts

from 
(
select 
uid,begindt,enddt,

case 
 when '18' between begindt and enddt  and '20' between begindt and enddt then 1
 when '18' between begindt and enddt  and '20' between maxbegin and maxenddt then 1
 when '18' between maxbegin and maxenddt  and '20' between maxbegin and maxenddt then 1
 else 0
end as flag

from 

(
select

uid,
begindt,
enddt,
last_value(begindt) over(partition by uid order by enddt rows between unbounded preceding and unbounded following) as maxbegin,
last_value(enddt) over(partition by uid order by enddt rows between unbounded preceding and unbounded following) as maxenddt

from t1
)  o

) o2

where flag=1



-- 然后，union 17号的3日后留存数  ，16号的4日后留存数 ，15号的5日后留存数.......

















