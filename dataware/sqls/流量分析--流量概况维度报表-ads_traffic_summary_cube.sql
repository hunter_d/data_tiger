/*
    ADS层： 流量概况维度分析报表
	--建模：
        将各种维度组合结果，统一成一张维度分析结果表
        其中包含所有的维度
		那么，不同维度组合的结果，将放入这张表的不同“维度组合”分区

*/


drop table if exists ads_traffic_summary_cube;
create table ads_traffic_summary_cube(
province string,
city string,
district string,
manufacture string,
osname string,
osver string,
appver string,

pv_amt int,
uv_amt int,
session_amt int,
avg_time_per_session double,
avg_session_per_user double,
avg_pv_per_user double,
avg_time_per_user double,
back_user_ratio double
)
partitioned by (dt string,dim string) -- 两级分区：日期，维度组合类型
stored as parquet


/*
 ETL实现，核心思想：
	
-- 维度组合情况
省   市   区   设备  系统  系统版本  app版本
1    0    0     0     0      0        0
0    1    0     0     0      0        0
..........

如果维度数有7个，维度的组合情况共有2的7次方种之多
如果各种维度组合都要进行预计算
则，查询任务可能会有2的7次方 次
如果每个查询任务都单独select一次，并插入结果表，则源表会被读取多达2的7次方次
效率低下
改造：使用hive的一个特性--》多重插入语法 multiple insert
*/
from dws_traffic_agg_user

insert into table ads_traffic_summary_cube partition(dt='2019-06-13',dim='0000000')
select 
'all',
'all',
'all',
'all',
'all',
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'


insert into table ads_traffic_summary_cube partition(dt='2019-06-13',dim='1000000')
select 
province,
'all',
'all',
'all',
'all',
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'
group by province

insert into table ads_traffic_summary_cube partition(dt='2019-06-13',dim='0100000')
select 
'all',
city,
'all',
'all',
'all',
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'
group by city

insert into table ads_traffic_summary_cube  partition(dt='2019-06-13',dim='0010000')
select 
'all',
'all',
district,
'all',
'all',
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'
group by district


insert into table ads_traffic_summary_cube   partition(dt='2019-06-13',dim='0001000')
select 
'all',
'all',
'all',
manufacture,
'all',
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'
group by manufacture


insert into table ads_traffic_summary_cube partition(dt='2019-06-13',dim='0000100')
select 
'all',
'all',
'all',
'all',
osname,
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'
group by osname


insert into table ads_traffic_summary_cube partition(dt='2019-06-13',dim='1000100')
select 
province,
'all',
'all',
'all',
osname,
'all',
'all',
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio
where dt = '2019-06-13'
group by province,osname
;


/*
  高级写法，利用cube函数来构建多维数据立方体
*/

drop table if exists ads_traffic_summary_cube;
create table ads_traffic_summary_cube(
province string,
city string,
district string,
manufacture string,
osname string,
osver string,
appver string,

pv_amt int,
uv_amt int,
session_amt int,
avg_time_per_session double,
avg_session_per_user double,
avg_pv_per_user double,
avg_time_per_user double,
back_user_ratio double
)
partitioned by (dt string)
stored as parquet




==》 利用高阶聚合函数cube可以一句话写完2的7次方中维度组合计算
insert into table tiger.ads_traffic_summary_cube partition(dt='2019-06-13')

select
province,city,district,county,osname,osver,appver,
sum(pv_cnts) as pv_amt,
count(1) as uv_amt,
sum(session_cnts) as session_amt,
sum(access_time_amt)/sum(session_cnts) as avg_time_per_session,
sum(session_cnts)/count(1) as avg_session_per_user,
sum(pv_cnts)/count(1) as avg_pv_per_user,
sum(access_time_amt)/count(1) as avg_time_per_user,
count(if(session_cnts>1,1,null))/count(1) as back_user_ratio,

lpad(bin(cast(GROUPING__ID as bigint)),7,'0') as dim

from  tiger.dws_traffic_agg_user t where dt = '2019-06-13'
-- 计算7个维度的所有组合
group by province,city,district,county,osname,osver,appver
-- 指定所需要的维度组合
-- grouping sets((province,osname),(province,appver),(osname,appver),(),(province))
with cube

;
















