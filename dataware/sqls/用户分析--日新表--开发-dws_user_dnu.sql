/*
    日新表：建模
	dws_user_dnu
	
*/
drop table if exists dws_user_dnu;
create table dws_user_dnu(
uid string,
province string,
city string,
district string,
osname string,
appver string,
week string comment '周', 
month string comment '月',
quarter string  comment '季'
)
partitioned by (dt string)
stored as parquet
;

/*
    ETL 开发
	源表： 前一日的历史记录 a  &   当天的日活表 b
	计算逻辑：  找出b中有，而a中没有的user
	
*/
-- 第一种写法：用in/not in 子句
-- 测试执行时长：192.077s
insert into table dws_user_dnu partition(dt='2019-06-13')
select
uid,
province,
city,
district,
osname,
appver

from dws_user_dau
where dt='2019-06-13' and uid not in (select uid from dws_user_hisu where dt='2019-06-12')
;

-- 第2种写法：用left join子句
-- 测试执行时长：82.68s
insert into table dws_user_dnu partition(dt='2019-06-13')

select 
buid as uid,
province,
city,
district,
osname,
appver,
week,
month,
quarter

from 

(
select
b.uid as buid,
a.uid as auid,
province,
city,
district,
osname,
appver,
b.week,
b.month,
b.quarter
b.dt as dt
from dws_user_dau b left join dws_user_hisu a
on b.dt='2019-06-13' and a.dt='2019-06-12' and b.uid = a.uid
) o

where o.auid is null

;






