/*
   历史留存记录表建模
   dws_user_hisu
*/
drop table if exists dws_user_hisu;
create table dws_user_hisu(
uid string,
ts_first string,
ts_last string
)
partitioned by (dt string)
stored as parquet
;

/*
   ETL开发
   源表：前一日的历史记录表 a +  今日的日活表 b(dws_user_dau)
   计算逻辑：
	  a full join b
	  对于能够关联上的，取a表的首次登陆时间（作为结果的首次登陆时间），b表的登录时间作为结果中的最后一次登录时间
	  对于a表有，而b表没有的，直接取a表的所有数据
	  对于a表没有，而b表有，则取b表的所有数据
*/

insert into table dws_user_hisu partition(dt='2019-06-13')
select
if(a.uid is null,b.uid,a.uid)  as uid,  --取uid
if(a.uid is not null,a.ts_first,b.dt) as ts_first, -- 取首次登陆时间：只要a表有，则首次登陆时间取自a表；否则取自b表
if(b.uid is not null,b.dt,a.ts_last) as ts_last -- 取最后登陆时间：只要b表有，则取自b表；否则取a表

from 
(select *
from 
dws_user_hisu 
where dt=date_sub('2019-06-13',1)) a

full join

(select *
from 
dws_user_dau 
where dt='2019-06-13') b
on a.uid=b.uid

;





