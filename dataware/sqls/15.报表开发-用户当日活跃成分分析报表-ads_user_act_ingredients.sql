/*
   用户当日活跃成分分析报表
   建模：竖表
   ads_user_act_ingredients
*/
drop table if exists ads_user_act_ingredients;
create table ads_user_act_ingredients(
dt string,
act_cnts int comment '当日活跃总数',
continuos int comment '连续天数',
cnts int comment '人数',
per double comment '人数占比'
)
stored as parquet;


/*
  ETL 开发
  计算逻辑：
	1.源表：活跃记录拉链表 dws_user_active_zip
	2.逻辑：先过滤出当日活跃的记录，然后分别累计（连续1天的，连续2天的....）人数即可


*/
with tmp as (select * from dws_user_active_zip where dt='2019-06-20' and  end_dt='9999-12-31')

insert into table ads_user_act_ingredients

select
-- 计算日期
'2019-06-20' as dt,
-- 当日活跃总数
count(1) as act_cnts,  
-- 连续天数类别
1 as continuos,  
-- 人数
count(1) as cnts,  -- 连续活跃1天及以上的
-- 占比
round(count(if(datediff('2019-06-20',begin_dt)>1,1,null))/count(1),4) as per
from   tmp


union all


select
-- 计算日期
'2019-06-20' as dt,
-- 当日活跃总数
count(1) as act_cnts,  -- 连续活跃2天及以上的
-- 连续天数类别
2 as continuos,
-- 人数
count(if(datediff('2019-06-20',begin_dt)>1,1,null)) as cnts,
-- 占比
round(count(if(datediff('2019-06-20',begin_dt)>1,1,null))/count(1),4) as per
from   tmp

union all

select
'2019-06-20' as dt,
count(1) as act_cnts,  -- 连续活跃3天及以上的
3 as continuos,
count(if(datediff('2019-06-20',begin_dt)>2,1,null)) as cnts,
round(count(if(datediff('2019-06-20',begin_dt)>2,1,null))/count(1),4) as per
from   tmp

union all

select
'2019-06-20' as dt,
count(1) as act_cnts,  -- 连续活跃4天及以上的
4 as continuos,
count(if(datediff('2019-06-20',begin_dt)>3,1,null)) as cnts,
round(count(if(datediff('2019-06-20',begin_dt)>3,1,null))/count(1),4) as per
from   tmp

union all

select
'2019-06-20' as dt,
count(1) as act_cnts, 
5 as continuos, -- 连续活跃5天及以上的
count(if(datediff('2019-06-20',begin_dt)>4,1,null)) as cnts,
round(count(if(datediff('2019-06-20',begin_dt)>4,1,null))/count(1),4) as per
from   tmp


union all

select
'2019-06-20' as dt,
count(1) as act_cnts,
6 as continuos, -- 连续活跃6天及以上的
count(if(datediff('2019-06-20',begin_dt)>5,1,null)) as cnts,
round(count(if(datediff('2019-06-20',begin_dt)>5,1,null))/count(1),4) as per
from   tmp

union all

select
'2019-06-20' as dt,
count(1) as act_cnts,
7 as continuos, -- 连续活跃7天及以上的
count(if(datediff('2019-06-20',begin_dt)>6,1,null)) as cnts,
round(count(if(datediff('2019-06-20',begin_dt)>6,1,null))/count(1),4) as per
from   tmp























