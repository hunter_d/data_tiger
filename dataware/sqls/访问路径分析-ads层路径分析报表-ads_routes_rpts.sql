/*
   访问路径分析：会话聚合表
   建模：ads_routes_agg_session
*/
drop table if exists ads_routes_rpts;
create table ads_routes_rpts(
url string,
step int,
pre_url string,
session_cnts int,
user_cnts int
) 
partitioned by (dt string)
stored as parquet
;

/*
   ETL 开发
   1.源表：访问路径分析明细表 dwd_routes_detail
   2.计算逻辑： groupby url,step,pre_url  求session个数
*/
insert into table ads_routes_rpts partition(dt='2019-06-15')
select
url,step,pre_url,
count(distinct sessionid) as session_cnts,
count(distinct uid) as user_cnts
from 
dwd_routes_detail
where dt='2019-06-15'
group by url,step,pre_url
;