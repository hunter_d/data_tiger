/*
 销售额统计报表：
 建模：ads_order_amt_cube
*/
create table ads_order_amt_cube(
tm_id bigint,
c1_name string,
c2_name string,
c3_name string,
sku_id bigint,
amt double
)
partitioned by (dt string)
stored as parquet;

/*
 ETL 开发
    1.源表：dws_order_detail
    2.计算逻辑：数据立方体构建
*/

insert into table ads_order_amt_cube partition(dt='2019-06-15')

select
tm_id,c1_name,c2_name,c3_name,sku_id,
sum(order_price) as amt
from dws_order_detail
group by tm_id,c1_name,c2_name,c3_name,sku_id
with cube;


