/*
   事件分析：DWD层事件明细表
   建模: dwd_event_detail
*/
drop table if exists dwd_event_detail;
create table dwd_event_detail(
event_type string comment '事件名称',
uid string,
sessionid string,
location string comment '事件所发生的页面',
event_dest string comment '事件的标的',
event_value double comment '事件的值(比如一个商品的价格，一个评分值)',
commit_time bigint comment '事件发生的时间戳'
)
partitioned by (dt string)
stored as parquet;

/*
   ETL 开发
   计算逻辑：
	1.源表：流量日志明细表 dwd_traffic_log
	2.逻辑：抽取
	现有的所有事件类型：
+---------------+
|    logtype    |
+---------------+
| act_click     |
| act_join      |
| act_register  |
| ad_click      |
| ad_show       |
| addcart       |
| favor         |
| order_pay     |
| order_sub     |
| pg_view       |
| rate          |
| search        |
| startup       |
| thumbup       |
+---------------+
	
*/
insert into table dwd_event_detail partition(dt='2019-06-16')
select
logtype as event_type,
uid,
sessionid,
eventmap['url'] as location,
case logtype
 when 'act_click'    then eventmap['actid']
 when 'act_join'     then eventmap['actid']
 when 'act_register' then eventmap['actid']
 when 'ad_click'     then eventmap['ad_id']
 when 'ad_show'      then eventmap['ad_id']
 when 'addcart'      then eventmap['skuid']
 when 'favor'        then eventmap['skuid']
 when 'order_pay'    then eventmap['order_id']
 when 'order_sub'    then eventmap['order_id']
 when 'pg_view'      then eventmap['skuid']
 when 'rate'         then eventmap['skuid']
 when 'search'       then eventmap['keywords']
 when 'startup'      then eventmap['']
 when 'thumbup'      then eventmap['skuid']
 else null
end as event_dest,

case logtype
 when 'order_pay'    then eventmap['total_money']
 when 'rate'         then eventmap['score']
 else null
 end as event_value,

commit_time

from  dwd_traffic_log
where dt='2019-06-16';

















