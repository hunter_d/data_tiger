/*  
   日活用户数，统计报表
   多维cube
   建模：ads_user_dau_cube
*/

drop table if exists ads_user_dau_cube;
create table ads_user_dau_cube(
u_cnts  int,
province string,
city string,
district string,
osname string,
appver string,
week string,
month string,
quarter string
)
partitioned by (dt string)
stored as parquet;

/*
	ETL开发
	源表： 日活明细表  dws_user_dau
	计算逻辑： 按各种维度组合聚合活跃用户数量

*/

insert into table tiger.ads_user_dau_cube partition (dt='2019-06-13')
select
count(uid) as u_cnts,
province,
city,
district,
osname,
appver,
week,month,quarter
from tiger.dws_user_dau where dt='2019-06-13'
group by province,city,district,osname,appver,week,month,quarter
with cube
;