/*
   DWS层，事件概况统计(按会话聚合) dws_event_overall_agg_session
*/
drop table if exists dws_event_overall_agg_session;
create  table dws_event_overall_agg_session(
event_type string,
uid string,
sessionid string,
cnts int
)
partitioned by (dt string)
stored as parquet;


/*
  ETL 开发
    1.源表，dwd层的事件明细表：dwd_event_detail
	2.计算逻辑： 分组聚合
*/

insert into table dws_event_overall_agg_session partition(dt='2019-06-16')
select
event_type,uid,sessionid,
count(1) as cnts

from dwd_event_detail
where dt='2019-06-16'
group by event_type,uid,sessionid
;

