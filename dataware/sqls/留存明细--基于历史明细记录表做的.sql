/*
   留存明细表
   建模：dws_user_retention_dtl
*/
drop table if exists dws_user_retention_dtl;
create table dws_user_retention_dtl(
dt_cacl string comment '报表计算日期',
dt_new  string comment '用户新增日期',
new_cnts  int comment '新用户数',
retention_cnts int comment '留存人数',
retention_days int comment '留存天数'
)
stored as parquet
;

/*
   ETL 计算逻辑
   源表：dws_user_hisu
*/
/*
各类留存单独计算，然后再union到一起
insert into table dws_user_retention_dtl

select
'2019-06-15' as dt_cacl,
-- 新增日：14号
date_sub('2019-06-15',1) as dt_new,
count(1) as new_cnts,
sum(if(ts_last='2019-06-15',1,0)) as retention_cnts,
1 as retention_days
from dws_user_hisu
-- 当前是15号，找新增日期为14号
where dt='2019-06-15' and ts_first=date_sub('2019-06-15',1)

union all

select
'2019-06-15' as dt_cacl,
-- 新增日：13号
date_sub('2019-06-15',2) as dt_new,
count(1) as new_cnts,
sum(if(ts_last='2019-06-15',1,0)) as retention_cnts,
2 as retention_days
from dws_user_hisu
-- 当前是15号，找新增日期为14号
where dt='2019-06-15' and ts_first=date_sub('2019-06-15',2)

*/


/*
   方式2：分组计算，一次性解决
*/

insert into table dws_user_retention_dtl

select
'2019-06-15' as dt_cacl,
ts_first,
count(1) as new_cnts,
count(if(ts_last='2019-06-15',1,null)) as retention_cnts,
datediff('2019-06-15',ts_first) as retention_days

from dws_user_hisu
where dt='2019-06-15' and ts_first != '2019-06-15'
and datediff('2019-06-15',ts_first)<31
group by ts_first
;




