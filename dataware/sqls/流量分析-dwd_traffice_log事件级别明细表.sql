/*
dwd_traffic_log	
先对ods流量日志做一个加工
为每条请求标识一个唯一用户id-- uid  
	-- 规则：按以下优先级来取：  account>imei>andoridid>deviceId
去除一些暂时不需要的字段
而且，请求时间可以转换成 年-月-日-时
*/	
drop table if exists dwd_traffic_log;
create table dwd_traffic_log(
uid string,
account string,
imei string,
osName string,
osVer string,
resolution string,
androidId string,
manufacture string,
deviceId string,
appid string,
appVer string,
release_ch string,
promotion_ch string,
areacode string,
longtitude double,
latitude double,
carrier string,
netType string,
cid_sn string,
sessionId string,
logType string,
commit_time bigint,
timestr string,
province string,
city string,
district string,
county string,
biz array<string>,
eventMap map<string,string>
)
partitioned by (dt string)
stored as parquet
;



/*
开发语句：

*/

insert into table dwd_traffic_log partition(dt='2019-06-13')

select
case 
when account is not null and account!='' then account
when imei is not null and imei!='' then imei
when androidId is not null and androidId!='' then androidId
when deviceId is not null and deviceId!='' then deviceId
end  as uid,
account,
imei ,
osName ,
osVer ,
resolution ,
androidId ,
manufacture ,
deviceId ,
appid ,
appVer ,
release_ch ,
promotion_ch ,
areacode ,
longtitude ,
latitude ,
carrier ,
netType ,
cid_sn ,
sessionId ,
logType ,
commit_time ,
from_unixtime(cast(commit_time/1000 as bigint),'yyyy-MM-dd HH:mm:ss') as timestr,
province ,
city ,
district ,
county ,
biz ,
eventMap 
from ods_traffic_log t
;





	
	