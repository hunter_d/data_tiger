/*
事件类型：
+---------------+
|    logtype    |
+---------------+
| act_click     |
| act_join      |
| act_register  |
| ad_click      |
| ad_show       |
| addcart       |
| favor         |
| order_pay     |
| order_sub     |
| pg_view       |
| rate          |
| search        |
| startup       |
| thumbup       |
+---------------+
  流量主题，dwd会话级别明细表：dwd_traffic_agg_session
  该表功能：
	将流量日志中的事件请求按照相同会话聚合，得到会话级别的明细
*/
-- 创建目标表模型
drop table if exists dwd_traffic_agg_session;
create table dwd_traffic_agg_session(
sessionId string,
ud string,
start_time bigint,
end_time bigint,
pv_cnts int,
province string,
city string,
district string,
county string,
biz array<string>,
manufacture string,
osname string,
osver string,
appver string

)
partitioned by (dt string)
stored as parquet
;

/*
  ETL步骤：
	对sessionId分组
	然后求各目标字段
*/
insert into table dwd_traffic_agg_session partition(dt='2019-06-13')

select
sessionId,
uid,
min(commit_time) as start_time,
max(commit_time) as end_time,
-- count(if(logType='pg_view',1,null)) as pv_cnts
sum(if(logType='pg_view',1,0))  as pv_cnts,
max(province) as province,
max(city) as city,
max(district) as district,
max(county) as county,
collect_set(biz)[0] as biz,
max(manufacture) as manufacture,
max(osname) as osname,
max(osver) as osver,
max(appver) as appver

from dwd_traffic_log
where dt = '2019-06-13'
group by sessionId,uid

;
