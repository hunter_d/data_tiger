/*
   整体趋势报表
   建模：ads_overall_trend
*/
create table ads_overall_trend(
dt string,
newuser_7d int,
newuser_7d_avg double,
newuser_30d int,
newuser_30d_avg double,
actuser_7d int,
actuser_7d_avg double,
actuser_30d int,
actuser_30d_avg double,

user_addup int,

newuser_1d_retention_7d_avg double,

usetime_avg_7d double
)
stored as parquet;


/*
   ETL计算
   计算逻辑：	
	1. 30日新用户数 ：从 dws_user_hisu  ，累计 新增日期在近30日内的 人数
	2. 7日新用户数 ：从 dws_user_hisu  ，累计 新增日期在近7日内的 人数
	3. 30日活跃用户数：从 dws_user_hisu  ，累计 最后活跃日在近30日内的 人数
	4. 7日活跃用户数： 从 dws_user_hisu  ，累计 最后活跃日在近7日内的 人数
	5. 累计用户数 ：  从 dws_user_hisu ，count总条数
	6. 次日留存率（7天范围内的每天次日留存）： 从 dws_user_retention_dtl 表中，查询新增日在7日范围内，且留存类别为1日留存的数据
	7. 使用时长（7日平均）：从 dws_traffic_agg_user表中，累加7日内的：所有人都使用时长/总人数
*/

select
a.dt,
a.newuser_7d,
a.newuser_7d_avg,
a.newuser_30d,
a.newuser_30d_avg,
a.actuser_7d,
a.actuser_7d_avg,
a.actuser_30d,
a.actuser_30d_avg,
a.user_addup,
b.newuser_1d_retention_7d_avg,
c.usetime_avg_7d

from 


(select
'2019-06-20' as dt,
count(if(ts_first between date_sub('2019-06-20',6) and '2019-06-20',1,null)) as newuser_7d,
count(if(ts_first between date_sub('2019-06-20',6) and '2019-06-20',1,null))/7 as newuser_7d_avg,
count(if(ts_first between date_sub('2019-06-20',29) and '2019-06-20',1,null)) as newuser_30d,
count(if(ts_first between date_sub('2019-06-20',29) and '2019-06-20',1,null))/30 as newuser_30d_avg,

count(if(ts_last between date_sub('2019-06-20',6) and '2019-06-20',1,null)) as actuser_7d,
count(if(ts_last between date_sub('2019-06-20',6) and '2019-06-20',1,null))/7 as actuser_7d_avg,
count(if(ts_last between date_sub('2019-06-20',29) and '2019-06-20',1,null)) as actuser_30d,
count(if(ts_last between date_sub('2019-06-20',29) and '2019-06-20',1,null))/30 as actuser_30d_avg,

count(1) as user_addup
from dws_user_hisu) a

join 

(select 
'2019-06-20' as dt,
sum(retention_cnts/new_cnts)/7 as newuser_1d_retention_7d_avg

from dws_user_retention_dtl
where dt_new > date_sub('2019-06-20',7) and retention_days=1
) b

on a.dt = b.dt

join

(
select
'2019-06-20' as dt,
sum(u_time_amt)/count(uid) as usetime_avg_7d

from 
(
select
uid ,
sum(access_time_amt) as u_time_amt -- 每个人的7天总时长

from  dws_traffic_agg_user
where dt > date_sub('2019-06-20',7)
group by uid
) o
) c

on a.dt = c.dt
;

















