/*
   用户活跃情况记录拉链表
   建模:dws_user_active_zip
*/
create table dws_user_active_zip(
uid string,
daynew string,
begin_dt string,
end_dt string
)
partitioned by (dt string)
stored as parquet;


/*
   ETL计算逻辑
   源表：日活表 dws_user_dau
   计算逻辑：
	将 日活表  full join 前日dws_user_active_zip
	将join结果分三类分别处理
*/
insert into table dws_user_active_zip partition(dt='2019-06-15')

select
if(a.uid is not null,a.uid,b.uid) as uid,
if(a.uid is not null,a.daynew,b.dt) as daynew,
if(a.uid is not null,a.begin_dt,b.dt) as begin_dt,
case 
 when a.uid is not null and b.uid is null and a.end_dt='9999-12-31' then date_sub('2019-06-14',1)
 when a.uid is not null and b.uid is null and a.end_dt!='9999-12-31' then a.end_dt
 when a.uid is null then '9999-12-31'
 else a.end_dt
end 
 as end_dt

from (select * from dws_user_active_zip where dt='2019-06-13') a 
full join (select * from dws_user_dau where dt='2019-06-14') b
on a.uid=b.uid


union all

select
a.uid,
b.daynew,
a.dt,
'9999-12-31' as end_dt

from 
(
select * from 
dws_user_dau where dt='2019-06-15') a

join

(
select
uid,daynew,begin_dt,end_dt
from 
(
select  
uid,
daynew,
begin_dt,
end_dt,
row_number() over(partition by uid order by end_dt desc) as rn
from dws_user_active_zip
where dt='2019-06-14'
) o
where rn=1
) b
on a.uid = b.uid
;



