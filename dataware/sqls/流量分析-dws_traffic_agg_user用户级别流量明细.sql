/*
  DWS：流量分析，用户级别明细表:dws_traffic_agg_user
*/
drop table if exists dws_traffic_agg_user;
create table dws_traffic_agg_user(
uid string COMMENT '用户唯一标识',
session_cnts int COMMENT '会话次数(访问次数)',
access_time_amt bigint  COMMENT '访问总时长',
pv_cnts int  COMMENT 'pv总数',
province string,
city string,
district string,
county string,
biz array<string>,
manufacture string,
osname string,
osver string,
appver string
)
partitioned by (dt string)
stored as parquet
;

/*
  ETL 开发
     逻辑： 按照uid分组，对各次session的数据进行聚合，各维度则取一个值
*/
insert into table dws_traffic_agg_user partition(dt='2019-06-13')
select
uid,
count(1) as session_cnts,
sum(end_time-start_time) as access_time_amt,
sum(pv_cnts) as pv_cnts,

max(province) as province,
max(city) as city,
max(district) as district,
max(county) as county,
collect_set(biz)[0] as biz,
max(manufacture) as manufacture,
max(osname) as osname,
max(osver) as osver,
max(appver) as appver

from dwd_traffic_agg_session
where dt='2019-06-13'
group by uid
;

























