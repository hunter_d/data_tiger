/*
   销售额分析：dws层明细表 
   建模：dws_order_detail
*/
create table dws_order_detail(
order_id bigint,
order_price double,
sku_num int,
total_amount double,
order_status int,
uid bigint,
birthday string,
gender string,
sku_id bigint,
sku_price double,
sku_name string,
tm_id bigint,
c3_name string,
c2_name string,
c1_name string
)
partitioned by (dt string)
stored as parquet;



/*
   ETL 开发 
   1.源表：ods层的n张订单相关表
   2.计算逻辑： join
*/
insert into table dws_order_detail partition(dt='2019-06-15')
select
od.order_id,
od.order_price,
od.sku_num,
oi.total_amount,
oi.order_status,
ui.id,
ui.birthday,
ui.gender,
si.id,
si.price,
si.sku_name,
si.tm_id,
c3.name,
c2.name,
c1.name
from  ods_order_detail od
join  ods_order_info oi on od.order_id=oi.id  -- 关联订单信息
join  ods_user_info ui on oi.user_id=ui.id  -- 关联用户信息
join  ods_sku_info si on od.sku_id=si.id -- 关联商品信息
join  ods_base_category3 c3 on  si.category3_id=c3.id -- 关联3及品类信息
join  ods_base_category2 c2 on c3.category2_id=c2.id --关联2级品类信息
join  ods_base_category1 c1 on c2.category1_id=c1.id
