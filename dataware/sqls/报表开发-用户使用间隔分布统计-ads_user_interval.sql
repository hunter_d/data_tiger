/*
   报表：用户使用间隔分布
   建模：

*/

create table ads_user_interval(
uid string,
interval_type string,
cnts int
)
partitioned by (dt string)
stored as parquet;


/*
   方式1：用spark任务来计算
 
*/


/*
   算完之后，导入结果数据到hive表
*/
hive -e "load data inpath '/user/hive/warehouse/tiger.db/ads_user_interval/dt=2019-06-19' into table tiger.ads_user_interval partition(dt='2019-06-19')"



/*
  方式2：用sql实现
*/

/*
   计算逻辑，见教案

*/

with tmp as 
(select
uid,
datediff(if(end_dt='9999-12-31','2019-06-19',end_dt),begin_dt) as day1_cnts,
end_dt,
lead(begin_dt,1,null)  over(partition by uid order by begin_dt) as jump_dt
from dws_user_active_zip
where dt='2019-06-19')

select 
uid,
interval_type,
sum(cnts)

from 

(
select
uid,
interval_type,
cnts 

from

(
select 
uid
,map('隔1天',day1_cnts,if(jump_dt is null,"NA",concat('隔',datediff(jump_dt,end_dt),'天')),1) as m
from tmp
)o

lateral view

explode(m) t as interval_type,cnts 
) o2
where interval_type!='NA'
group by uid,interval_type