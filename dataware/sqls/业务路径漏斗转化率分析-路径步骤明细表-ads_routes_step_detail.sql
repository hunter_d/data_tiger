/*
   业务路径转化步骤明细表
   建模：ads_routes_step_detail
   
*/
create table ads_routes_step_detail(
uid string,
sessionid string,
isstep1 int,
isstep2 int,
isstep3 int,
isstep4 int
)
partitioned by (dt string)
stored as parquet;


/*
   ETL开发
    1.源表：事件明细表dwd_event_detail
	2.计算逻辑
		a.将各个页面location，通过正则匹配，来转换成事件的步骤(首页，类别页，详情页)
		b.按uid和sessionid分组，将事件id，变换后的location，收集到一起
		c.判断聚合起来的一次会话中，是否包含指定的各个步骤，是，就为这个步骤标记1，否则标记0
   
*/
-- 样例数据
/*
a,s01,pgview,/p                                      
a,s01,pgview,/a                                      
a,s01,thumbup,/a                                     
a,s01,favor,/a
a,s01,pgview,/b
a,s01,pgview,/a
a,s01,pgview,/b
a,s01,pgview,/c
a,s01,rate,/c
a,s01,add_cart,/c

b,s02,pgview,/a
b,s02,pgview,/b
b,s02,pgview,/a
b,s02,pgview,/b
b,s02,pgview,/c
b,s02,rate,/c


b,s02,pgview,/x
b,s02,pgview,/y
b,s02,rate,/y
b,s02,rate,/b
*/

----》》》》》》----
a  1  1  1  1
b  1  1  1  0
c  0  0  0  0


---》》》》》》-----
with tmp as(
select 
uid,
sessionid,
collect_set(event_type) as types,
collect_set(location) as locations
from 
(
select
uid,
sessionid,
event_type,
case
 when trim(regexp_extract(location,'(/index.html)(\\.*)',1))!='' then '首页'
 when trim(regexp_extract(location,'(/cat\\d+)(\\?\\.*)',1))!='' then '类别页'
 when trim(regexp_extract(location,'(/cat\\d+/\\w+\\.html)(\\.*)',1))!='' then '详情页'
 else '其他'
end as location
from dwd_event_detail
where dt='2019-06-15'
) o
group by uid,sessionid
)

insert into table ads_routes_step_detail partition(dt='2019-06-15')
select 
uid,
sessionid,
if(array_contains(locations,'首页'),1,0) as isStep1,
if(array_contains(locations,'类别页'),1,0) as isStep2,
if(array_contains(locations,'详情页'),1,0) as isStep3,
if(array_contains(types,'add_cart'),1,0) as isStep4

from tmp;