package cn.doitedu.common.utils

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession


/**
  * @date: 2019/7/2
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 各类字典数据加载工具
  */
object DictLoader {

  /**
    * 加载idmapping 字典
    * @param spark
    * @param path
    * @return
    */
  def loadIDM(spark: SparkSession, path: String):RDD[(String,String)] = {

    val idm = spark.read.textFile(path)
    idm.rdd.flatMap(line => {
      val arr = line.split(":")
      val gid = arr(0)
      val idsList = arr(1).split(",", -1).toList
      for (id <- idsList) yield (id, gid)
    })

  }

  /**
    * 加载url内容知识字典
    * @param spark
    * @param path
    * @return
    */
  def loadUrlContent(spark: SparkSession, path: String): RDD[(String, (String, String, String, String, String))] = {

    val rdd = spark.read.textFile(path).rdd
    rdd.map(line=>line.split(",",-1))
      .filter(_.size>=3)
      .map(arr=>{
        val url = arr(0)
        val tmp = arr(1).split(" ")
        val cat1 = tmp(0)
        val cat2 = tmp(1)
        val cat3 = tmp(2)
        val brand = tmp(3)
        val title = arr(2)

        (url,(cat1,cat2,cat3,brand,title))
      })

  }



}
