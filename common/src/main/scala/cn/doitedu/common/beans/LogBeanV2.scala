package cn.doitedu.common.beans

/**
 * @date: 2019/6/14
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description: 增强版logbean，添加了地理位置信息字段、商圈信息字段
 */
case class LogBeanV2 (
                  val cookieid :String,
                  val account :String,
                  val imei :String,
                  val osName :String,
                  val osVer :String,
                  val resolution :String,
                  val androidId :String,
                  val manufacture :String,
                  val deviceId :String,
                  val appid :String,
                  val appVer :String,
                  val release_ch :String,
                  val promotion_ch :String,
                  val areacode :String,
                  val longtitude :Double,
                  val latitude :Double,
                  val carrier :String,
                  val netType :String,
                  val cid_sn :String,
                  val ip :String,
                  val sessionId :String,
                  val logType :String,
                  val commit_time:Long,
                  val province:String,
                  val city:String,
                  val district:String,
                  val county:String,
                  val biz:List[String],
                  val eventMap: Map[String, String]
                )
