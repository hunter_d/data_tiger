package cn.doitedu.classify.bayes

import cn.doitedu.common.utils.SparkUtils
import org.apache.spark.ml.classification.NaiveBayes
import org.apache.spark.ml.feature.{HashingTF, IDF, Tokenizer}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.{DataTypes, StructType}

object BayesDemo {

  def main(args: Array[String]): Unit = {


    // 读取数据，抽取字段
    val spark = SparkUtils.getSparkSession("word2vec")


    val schema = new StructType()
      .add("docid", DataTypes.StringType)
      .add("label", DataTypes.DoubleType)
      .add("doc", DataTypes.StringType)

    // docid   label   doc
    val df = spark.read.schema(schema).option("header", "true").csv("recommend/bayes_demo_data/sample.txt")


    //  文本分词、抽取关键词 ： HanLp


    //  文本分词
    val tokenizer = new Tokenizer().setInputCol("doc").setOutputCol("words")
    val wordDf = tokenizer.transform(df)

    // 构造一个文本向量化的算法模型（本算法基于hash来定维度索引，用tf来作为词的特征值）
    val hashingTF = new HashingTF().setInputCol("words").setOutputCol("tfvec").setNumFeatures(10000)
    val tfResult: DataFrame = hashingTF.transform(wordDf)


    // 基于TF特征值，计算TF-IDF特征值
    val idf = new IDF().setInputCol("tfvec").setOutputCol("tfidf_vec")
    val idfModel = idf.fit(tfResult)
    val idfResult = idfModel.transform(tfResult).drop("doc", "tfvec", "words")



    /*tfResult.show(10,false)
    idfResult.show(10,false)
    */

    //  用朴素贝叶斯算法进行模型训练
    val bayes = new NaiveBayes().setFeaturesCol("tfidf_vec").setLabelCol("label").setPredictionCol("prediction").setSmoothing(1.0)
    val bayesModel = bayes.fit(idfResult)

    //  用模型来预测目标的分类 ，直接对原来的样本数据进行预测
    val predicitonDF = bayesModel.transform(idfResult)

    // 用训练好的模型来对测试数据进行预测
    val schema_test = new StructType()
      .add("docid", DataTypes.StringType)
      .add("doc", DataTypes.StringType)

    val testDF = spark.read.schema(schema_test).csv("recommend/bayes_demo_data/test.txt")
    val testWordsDF = tokenizer.transform(testDF)
    val testTFResult = hashingTF.transform(testWordsDF)
    val testIDFModel = idf.fit(testTFResult)
    val testIDFResult = testIDFModel.transform(testTFResult)

    val testPrediction = bayesModel.transform(testIDFResult)

    testPrediction.show(10, false)


    spark.close()

  }

}
