package cn.doitedu.classify.bayes

import cn.doitedu.common.utils.SparkUtils
import com.hankcs.hanlp.HanLP
import org.apache.hadoop.util.hash.Hash
import org.apache.spark.ml.classification.NaiveBayes
import org.apache.spark.ml.feature.{HashingTF, IDF}
import org.apache.spark.sql.{DataFrame, Row}

/**
 * @date: 2019/7/19
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  用户评分矩阵建设中的一环：  将用户对物品的评论进行分类，以便得到评论得分
 */
object CommentClassify {

  def main(args: Array[String]): Unit = {


    val spark = SparkUtils
      .getSparkSession("classify")
    import spark.implicits._


    // 加载样本语料
    val goodDs = spark.read.textFile("G:\\testdata\\comment\\good")
    val generalDs = spark.read.textFile("G:\\testdata\\comment\\general")
    val poorDs = spark.read.textFile("G:\\testdata\\comment\\poor")


    // 给3类样本标注类别标签（0：好评，1：中评，2：差评）
    val goodDF = goodDs.map(line=>{

      // 分词
      import scala.collection.JavaConversions._
      val words: Array[String] = HanLP.segment(line).map(term=>term.word).filter(_.size>1).toArray

      (0.0,words)
    }).toDF("label","words")


    val generalDF = generalDs.map(line=>{

      // 分词
      import scala.collection.JavaConversions._
      val words: Array[String] = HanLP.segment(line).map(term=>term.word).filter(_.size>1).toArray

      (1.0,words)
    }).toDF("label","words")



    val poorDF = poorDs.map(line=>{

      // 分词
      import scala.collection.JavaConversions._
      val words: Array[String] = HanLP.segment(line).map(term=>term.word).filter(_.size>1).toArray

      (2.0,words)
    }).toDF("label","words")


    val sampleDF = goodDF.union(generalDF).union(poorDF)

    // 将样本数据进行特征(tf-idf)向量化(hashingTF)
    val hashingTF = new HashingTF().setInputCol("words").setOutputCol("tfvec").setNumFeatures(3000000)
    val tfResult = hashingTF.transform(sampleDF)

    val idf = new IDF().setInputCol("tfvec").setOutputCol("tfidf_vec")
    val iDFModel = idf.fit(tfResult)
    val idfVecResult = iDFModel.transform(tfResult)


    // 构建bayes算法模型
    val bayes = new NaiveBayes().setSmoothing(1.0).setFeaturesCol("tfidf_vec").setLabelCol("label").setPredictionCol("prediction")
    val bayesModel = bayes.fit(idfVecResult)


    // 训练好的模型可以保存到hdfs文件中
    //bayesModel.save("")

    // 下次使用可以直接加载，而不用重新训练
    //NaiveBayes.load("")

    /**
      * 用训练好的模型来预测我们的评论数据集
      */

    // 1. 将我们自己的评论数据加载并向量化
    val cmtDS: DataFrame = spark.read.option("hader","true").csv("recommend/rec-test-data/cb_input/u.comment.dat")
    val cmtWordsDF = cmtDS.map({
      case Row(gid:String,pid:String,comment:String)=>{

        import scala.collection.JavaConversions._
        val words = HanLP.segment(comment).map(term=>term.word).filter(_.size>1).toArray

        (gid,pid,words)
      }

    }).toDF("gid","pid","words")


    // 将关键词进行TF向量化
    val cmtTFResult = hashingTF.transform(cmtWordsDF)
    // 将TF特征转成TF-IDF特征值
    val cmtIDFModel = idf.fit(cmtTFResult)
    val cmtIDFResult = cmtIDFModel.transform(cmtTFResult)


    // 2. 然后用训练好的贝叶斯模型来预测分类结果
    val prediction = bayesModel.transform(cmtIDFResult).select("gid","pid","prediction")

    prediction.write.parquet("recommend/rec-test-data/comment_classify");


    prediction.show(10,false);


    spark.close()

  }

}
