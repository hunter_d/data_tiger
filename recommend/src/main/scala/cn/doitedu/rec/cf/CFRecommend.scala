package cn.doitedu.rec.cf

import cn.doitedu.common.utils.SparkUtils
import org.apache.log4j.{Level, Logger}
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.sql.Row

/**
 * @date: 2019/7/20
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  协同过滤算法推荐程序
 */
object CFRecommend {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val spark = SparkUtils.getSparkSession("cf")
    import spark.implicits._

    // 加载用户物品评分矩阵
    val df = spark.read.parquet("recommend/rec-test-data/ui_rate_matrix")
    val dfdouble = df.map({
        case Row(gid:String,pid:String,rate:Double) => (gid,pid,gid.hashCode.toDouble,pid.hashCode.toDouble,rate)
      }
    ).toDF("gid","pid","giddouble","piddouble","rate")


    // 构造一个ALS算法模型
    val als = new ALS()
      .setUserCol("giddouble")
      .setItemCol("piddouble")
      .setRatingCol("rate")
      .setRegParam(0.01)
      .setColdStartStrategy("drop")
      .setMaxIter(5)
    // 训练模型：主要拆解两个特征矩阵
    val model = als.fit(dfdouble)

    // 为每个item推荐2个用户
    val recForItem = model.recommendForAllItems(2)

    // 为每个用户推荐两个item
    val recForUser = model.recommendForAllUsers(2)

    recForItem.show(20,false)
    recForItem.coalesce(1).write.parquet("recommend/rec-test-data/cf_rec_out_for_item")
    recForUser.show(20,false)
    recForUser.coalesce(1).write.parquet("recommend/rec-test-data/cf_rec_out_for_user")




    spark.close()
  }

}
