package cn.doitedu.rec.cb

import cn.doitedu.common.utils.{SparkUtils}
import org.apache.spark.sql.{DataFrame, SparkSession}


/**
 * @date: 2019/7/20
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description:  为用户计算基于内容相似度的推荐矩阵
  *              将用户评分矩阵   join   物品相似度矩阵
  *              以便于将用户的评分去加权物品的相似度
  *              最后对加权后的得分进行排序取topn
  *
  *   在真实生产实践中要做一些取舍：
  *       如果某个商品在某用户的浏览记录中，时间已经很久远，那么用户对这个物品的喜好的衡量价值已经没有意义了
  *       所以，如果要做得更好一点，应该将用户对物品的喜好评分按时间加权衰减
  *
 */
object CBRecommendMatrix {
  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkUtils.getSparkSession("cb_rec")

    // 加载物品相似度矩阵  pidA  pidB  ogsim  cosim
    val itemsim: DataFrame = spark.read.parquet("rec_sys/rec-test-data/out-itemsim")

    // 加载user-item偏好（评分）矩阵  gid  pid  prefer
    val uiprefer: DataFrame = spark.read.parquet("rec_sys/rec-test-data/out-ui-prefer")


    val joined = uiprefer.join(itemsim, uiprefer("pid") === itemsim("pidA")).where("pid != pidB")

    joined.show(40,false)



    // 对相似度得分进行加权计算，得到推荐得分矩阵
    val recMatrix = joined.selectExpr("gid", "pidB as pid", "prefer * cosim as rec_score")


    // 对推荐得分矩阵给每个用户取topn个推荐商品
    recMatrix.createTempView("rec_mat")

    val topnRec = spark.sql(
      """
        |select gid,pid,rec_score
        |
        |from
        |(
        |select
        |gid,
        |pid,
        |rec_score,
        |row_number() over(partition by gid order by rec_score desc) as rn
        |from rec_mat
        |) o
        |
        |where o.rn<=3
        |
      """.stripMargin)



    topnRec.show(30,false)

    spark.close()


  }

}
