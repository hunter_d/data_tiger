package cn.doitedu.rec.cb

import cn.doitedu.common.utils.SparkUtils
import org.apache.spark.sql.types.{DataTypes, StructType}

/**
 * @date: 2019/7/20
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description: 用户对物品的评分矩阵计算
 */
object UserRateMatrix {


  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("ui_matrix")
    import spark.implicits._
    import org.apache.spark.sql.functions._

    val schema = new StructType().add("gid",DataTypes.StringType)
        .add("logtype",DataTypes.StringType)
        .add("event",DataTypes.createMapType(DataTypes.StringType,DataTypes.StringType))


    // 加载用户行为记录数据
    val u_action = spark.read.schema(schema).json("recommend/rec-test-data/cb_input/u.profile.dat")


    // 加载用户评论贝叶斯分类结果数据
    val u_comment = spark.read.parquet("recommend/rec-test-data/comment_classify")



    // 整理成：  用户  物品  评分
    //u_action.show(10,false)

    u_action.createTempView("action")

    val score_action = spark.sql(
      """
        |
        |select
        |gid,
        |
        |event['pid'] as pid,
        |
        |case logtype
        | when 'pv' then 1
        | when 'add_cart' then 2
        | when 'rate' then event['score']-3
        | else 0
        |end as score
        |
        |from action
        |
      """.stripMargin)


    //u_comment.show(10,false)
    val score_cmt = u_comment.selectExpr("gid","pid","case prediction when 0.0 then 2 when 1.0 then 0 when 2.0 then -2 end as score")


    // score_action.show(10,false)
    // score_cmt.show(10,false)


    score_action.union(score_cmt).groupBy("gid","pid").agg(sum($"score").as("rate"))
      .coalesce(1)
      .write
      .parquet("recommend/rec-test-data/ui_rate_matrix")

    spark.close()




  }



}
