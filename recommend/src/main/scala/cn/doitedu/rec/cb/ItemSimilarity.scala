package cn.doitedu.rec.cb

import cn.doitedu.common.utils.SparkUtils
import cn.doitedu.rec.util.CalcSimilaryMatrix
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.ml.feature.{HashingTF, IDF}
import org.apache.spark.ml.linalg
import org.apache.spark.sql.SparkSession

/**
  * @date: 2019/7/20
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 计算物品相似度矩阵
  *               使用的算法是余弦相似度
  */
object ItemSimilarity {

  def main(args: Array[String]): Unit = {

    val spark = SparkUtils.getSparkSession("itemsimilarity")
    import spark.implicits._


    val df = spark.read.option("header", "true").csv("recommend/rec-test-data/cb_input/item.profile.dat")

    // 数据中的某些 “值特别离散的字段” 进行区间化
    // 加载价格区间字典
    val priceDictRdd = spark.read.textFile("recommend/rec-test-data/cb_input/price-level.dict").rdd

    val priceDictMap: collection.Map[String, Array[Double]] = priceDictRdd.map(line => {
      val split = line.split(",")
      val cat3 = split(0)
      val segs = split(1).split("-").map(_.toDouble)
      (cat3, segs)
    }).collectAsMap()

    // 广播字典
    val bc: Broadcast[collection.Map[String, Array[Double]]] = spark.sparkContext.broadcast[collection.Map[String, Array[Double]]](priceDictMap)


    // 转价格区间，拼接特征字符串
    val pStrfeature = df.rdd.map(row => {

      val dict = bc.value


      val pid = row.getAs[String]("pid")
      val price = row.getAs[String]("price").toDouble
      val cat1 = row.getAs[String]("cat1")
      val cat2 = row.getAs[String]("cat2")
      val cat3 = row.getAs[String]("cat3")
      val kwds = row.getAs[String]("kwds")


      // 获取价格区间分界点
      val arr = dict.get(cat3).get
      var priceLevel = ""
      if (price < arr(0)) {
        priceLevel = "低端品"
      }

      if (price >= arr(0) && price < arr(1)) {
        priceLevel = "中端品"
      }


      if (price >= arr(1) && price < arr(2)) {
        priceLevel = "高端品"
      }

      if (price >= arr(2)) {
        priceLevel = "奢侈品"
      }

      (pid, (priceLevel + " " + cat1 + " " + cat2 + " " + cat3 + " " + kwds).split(" "))
    }).toDF("pid", "properties")


    // 字符串特征 --》 tf-idf特征值向量
    val tf = new HashingTF().setInputCol("properties").setOutputCol("tf_vec").setNumFeatures(100000)
    val tfFeature = tf.transform(pStrfeature)

    val iDFModel = new IDF().setInputCol("tf_vec").setOutputCol("idf_vec").fit(tfFeature)
    val idfFeature = iDFModel.transform(tfFeature).select("pid", "idf_vec")

    //idfFeature.show(10,false)


    /**
      * 计算相似度矩阵
      */

    val joinedRdd = idfFeature
      .withColumnRenamed("pid", "apid")
      .withColumnRenamed("idf_vec", "aidf")
      .crossJoin(idfFeature)
      .where("apid > pid")
        .rdd
      .map(row=>{
        (
          row.getAs[String]("apid"),
          row.getAs[linalg.Vector]("aidf"),
          row.getAs[String]("pid"),
          row.getAs[linalg.Vector]("idf_vec")
        )

      })

    val similarityMatrix = CalcSimilaryMatrix.calcSimilary(joinedRdd)


    similarityMatrix.toDF("pid1","pid2","cosin","eu")
      //  .show(30)
        .drop("eu")
        .write
        .parquet("recommend/rec-test-data/item-sim-matrix")


    spark.close()


  }

}
