package cn.doitedu.rec.util

import org.apache.spark.ml.linalg
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.rdd.RDD

/**
  * @date: 2019/7/20
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 计算相似度矩阵
  */
object CalcSimilaryMatrix {

  def calcSimilary(rdd: RDD[(String, linalg.Vector, String, linalg.Vector)]): RDD[(String, String, Double, Double)] = {

    val x: RDD[(String, String, Double, Double)] = rdd.map(tp => {

      val pid1 = tp._1
      val pid2 = tp._3

      val vec1 = tp._2
      val vec2 = tp._4


      val cosin = cosinSimilarity(vec1, vec2)

      val eu = euSimilarity(vec1, vec2)

      (pid1, pid2, cosin, eu)
    })
    x
  }


  /**
    * 余弦相似度
    *
    * @param vec1
    * @param vec2
    * @return
    */
  def cosinSimilarity(vec1: linalg.Vector, vec2: linalg.Vector): Double = {

    val v1 = vec1.toSparse
    val v2 = vec2.toSparse

    val fenmu1: Double = v1.values.map(Math.pow(_, 2)).sum
    val fenmu2 = v2.values.map(Math.pow(_, 2)).sum


    val v1i = v1.indices
    val v2i = v2.indices

    // 求两个稀疏向量的共同脚标
    val sharedIndices = v1i.toSet.intersect(v2i.toSet)


    // 计算分子
    val fenzi: Double = sharedIndices.map(i => v1.values(v1i.indexOf(i)) * v2.values(v2i.indexOf(i))).sum


    // 返回余弦相似度
    fenzi / (Math.sqrt(fenmu1) * Math.sqrt(fenmu2))

  }


  /**
    * 欧氏距离相似度
    *
    * @param vec1
    * @param vec2
    * @return
    */
  def euSimilarity(vec1: linalg.Vector, vec2: linalg.Vector): Double = {

    1 / Math.sqrt(Vectors.sqdist(vec1, vec2))

  }


  /**
    * 测试
    * @param args
    */
  def main(args: Array[String]): Unit = {

    val v1 = Vectors.sparse(4,Array(0,1),Array(2,2))
    val v2 = Vectors.sparse(4,Array(1,2),Array(1,1))
    println(cosinSimilarity(v1, v2))

  }

}
