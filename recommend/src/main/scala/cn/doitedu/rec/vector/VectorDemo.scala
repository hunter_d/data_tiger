package cn.doitedu.rec.vector

import cn.doitedu.common.utils.SparkUtils
import org.apache.spark.ml.feature.{MaxAbsScaler, MinMaxScaler, MinMaxScalerModel, Normalizer, StandardScaler}
import org.apache.spark.ml.linalg
import org.apache.spark.ml.linalg.Vectors
import org.apache.spark.rdd.RDD

/**
 * @date: 2019/7/19
 * @site: www.doitedu.cn
 * @author: hunter.d 涛哥
 * @qq: 657270652
 * @description: 向量就是一个数字序列，比如 ---  3维向量：(3,2,9)    9维向量 (0,1,0,0,1,1,0,0,1)
  *              向量也可以理解成空间中的一个点；
 */
object VectorDemo {

  def main(args: Array[String]): Unit = {

    /**
      * 自定义的向量模型设计
      */
    // 稠密向量
    val vec1 = Array(3,2,9)
    val vec2 = Array(4,6,8)
    // 也可以用稀疏的方式来标识
    val sparse = (3,Array(0,1,2),Array(3,2,9))

    val r2ms = vec1.zip(vec2).map(tp=>Math.pow(tp._1-tp._2,2)).sum
    val euDistance = Math.sqrt(r2ms);


    // 假设还有这样的向量：稀疏向量
    // (0,0,0,0,0,0,0,87,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,0,45,0,.....)
    val sparseVec1 =  (1024,Array(7,25,34),Array(87,9,45))
    val sparseVec2 =  (1024,Array(17,25,34,66),Array(19,9,45,65))
    // 可以用稠密的方式来标识
    val denseVec = Array(0,0,0,0,0,0,0,87,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0,0,0,0,45,0)



    /**
      * spark mllib包中的向量模型  DenseVector
      */
    val vector1: linalg.Vector = Vectors.dense(Array(3.0,2.0,9.0))

    // （0,0,0,6,0,0,0,5）   SparseVector
    val vector2 = Vectors.sparse(8,Array(3,7),Array(6,5))


    val a1 = Array(3.0,14.0)
    val a2 = Array(1.0,12.0)
    val v1 = Vectors.dense(a1)
    val v2 = Vectors.dense(a2)
    println(Vectors.norm(v1, 2.0))

    val spark = SparkUtils.getSparkSession("x")
    import spark.implicits._
    val data = spark.sparkContext.makeRDD(Seq(a1,a2)).map(a=>("a",Vectors.dense(a))).toDF("u","vec")


    /**
      * 这里是4种向量规范化算法
      */

    val norm = new Normalizer().setInputCol("vec").setOutputCol("norm").setP(1)
    norm.transform(data).show(10,false)


    val sd = new StandardScaler().setInputCol("vec").setOutputCol("stand").setWithMean(true).setWithStd(true)
    sd.fit(data).transform(data).show(10,false)

    val minscalar = new MinMaxScaler().setInputCol("vec").setOutputCol("minmax")
    minscalar.fit(data).transform(data).show(10,false)


    val maxscalar = new MaxAbsScaler().setInputCol("vec").setOutputCol("maxabs")
    maxscalar.fit(data).transform(data).show(10,false)



    // 计算两个向量之间的欧氏距离
    val vec1Array: Array[Double] = vector1.toArray
    val vec2Array: Array[Double] = vector2.toArray
    val r2 = vec1Array.zip(vec2Array).map(tp=>Math.pow(tp._1-tp._2,2)).sum
    val ed = Math.sqrt(r2);

    //
    //val rr2 = Vectors.sqdist(vector1,vector2)
    //val ed2 = Math.sqrt(rr2)







  }

}
