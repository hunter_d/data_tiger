package cn.doitedu.rec.vector

import org.apache.spark.ml.linalg
import org.apache.spark.ml.linalg.Vectors

import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * @date: 2019/7/19
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 事物向量化的演示
  *               基于用户行为统计、标签报表，计算用户之间的行为相似度
  */
object Item2Vector {

  def main(args: Array[String]): Unit = {

    val lines = Source.fromFile("recommend/item2vec-data/customer.rpts").getLines()

    val vecs = new ListBuffer[(String, linalg.Vector)]()

    var i = 0
    for (line <- lines) {
      i = i + 1
      if (i != 1) {

        val split = line.split(",")
        val gid: String = split(0)
        val featureArr: Array[Double] = split.tail.map(_.toDouble)
        val vector: linalg.Vector = Vectors.dense(featureArr)
        vecs += ((gid, vector))
      }


    }

    for (i <- 0 until (vecs.size - 1); j <- i + 1 until (vecs.size)) {
      val t1 = vecs(i)
      val t2 = vecs(j)

      val gid1 = t1._1
      val gid2 = t2._1

      val d = Math.sqrt(Vectors.sqdist(t1._2, t2._2))
      println(s"${gid1} 与 ${gid2} 的欧氏距离为： ${d}")
    }


  }

}
