import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class ExtractInfoTest {

    public static void main(String[] args) throws IOException {

       //https://item.jd.com/31829200835.html
        Document doc = Jsoup.connect("https://item.jd.com/31829200835.html").get();

        Elements eles = doc.select("div.crumb.fl.clearfix");
        Element div = eles.get(0);
        Elements atags = div.select("a");

        StringBuilder sb = new StringBuilder();

        for(Element a : atags){
            sb.append(a.text()).append(" ");
        }

        System.out.println(sb.toString());


        System.out.println(div.getElementsByAttribute("title"));


    }

}
