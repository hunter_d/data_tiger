package cn.doitedu.crawler

import java.io.{BufferedWriter, File, FileWriter}
import java.util

import org.jsoup.Jsoup
import org.jsoup.select.Elements

/**
  * @date: 2019/6/30
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 京东户外冲锋衣品类爬虫程序
  */
object JdCrawlerOutsideUrlExtractor {

  def main(args: Array[String]): Unit = {

    new File("crawler/data/1318-2628-12123/").mkdirs()
    val bw = new BufferedWriter(new FileWriter("crawler/data/1318-2628-12123/urls.lst"))


    for (i <- 1 to 253) {
      val document = Jsoup.connect("https://list.jd.com/list.html?cat=1318,2628,12123&page=" + i).get()

      /*val elements: Elements = document.getElementsByTag("ul")
    println(elements.size())*/

      /*val elements = document.getElementsByClass("gl-warp clearfix")
    println(elements.get(0))*/


      val liLst = document.getElementById("plist").getElementsByTag("li")
      /*println(liLst.get(0).getElementsByAttribute("title").get(1).attr("href"))*/

      import scala.collection.JavaConversions._

      for (ele <- liLst) {
        val titles = ele.getElementsByAttribute("title")
        if (titles.size() > 1) {
          val href = titles.get(1).attr("href")

          bw.write("https:" + href)
          bw.newLine()
        }
      }
      bw.flush()

    }

    bw.close()

  }

}
