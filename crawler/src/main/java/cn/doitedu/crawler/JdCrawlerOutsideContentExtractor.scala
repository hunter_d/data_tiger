package cn.doitedu.crawler

import java.io.{BufferedReader, FileReader}
import java.util
import java.util.concurrent.Executors

import org.jsoup.Jsoup

/**
  * @date: 2019/6/30
  * @site: www.doitedu.cn
  * @author: hunter.d 涛哥
  * @qq: 657270652
  * @description: 京东户外冲锋衣品类商品信息提取程序
  */
object JdCrawlerOutsideContentExtractor {

  def main(args: Array[String]): Unit = {

    val urlList = new util.ArrayList[String]()

    val br = new BufferedReader(new FileReader("crawler/data/1318-2628-12123/urls.lst"))
    var line: String = null
    var i = 0
    var flag = true
    while (flag) {
      line = br.readLine()
      if(line==null) flag =false
      urlList.add(line)
      i = i+1
      println(i)
    }


    val service = Executors.newCachedThreadPool()

    flag = true
    var index = 0

    while(flag){
      var end = (index+1)*1500-1
      if((index+1)*1500-1>=urlList.size()-1){
        end = urlList.size()-1
        flag = false
      }
      service.execute(new ContentExtractTask(urlList,index*1500,end,"crawler/data/content_info"))

      println(index)
      index = index +1
    }
  }
}
