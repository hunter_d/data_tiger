package cn.doitedu.crawler;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.List;

public class ContentExtractTask implements Runnable {

    private List<String> urlList;
    private int start;
    private int end;
    private String savePath;

    public ContentExtractTask(List<String> urlList, int start, int end,String savePath) {
        this.urlList = urlList;
        this.start = start;
        this.end = end;
        this.savePath = savePath;

    }

    @Override
    public void run() {

        try {

            BufferedWriter bw = new BufferedWriter(new FileWriter(savePath + "/" + Thread.currentThread().getId() + ".csv"));

            for (int i = start; i <= end; i++) {


                try {
                    Document doc = Jsoup.connect(urlList.get(i)).get();

                    Elements eles = doc.select("div.crumb.fl.clearfix");
                    Element div = eles.get(0);

                    /**
                     * 抽取品类信息
                     */
                    Elements atags = div.select("a");
                    StringBuilder sb = new StringBuilder();
                    for (Element a : atags) {
                        sb.append(a.text()).append(" ");
                    }
                    // 品类信息
                    String categoryInfo = sb.toString().trim();


                    /**
                     * 抽取商品页面标题
                     */
                    String title = div.getElementsByAttribute("title").get(0).text();

                    /**
                     * 输出结果
                     */
                    bw.write(urlList.get(i)+","+categoryInfo +"," + title);
                    bw.newLine();
                    bw.flush();


                }catch (Exception e){

                }


            }
            bw.close();


        }catch (Exception e){
            //e.printStackTrace();
        }


    }
}
